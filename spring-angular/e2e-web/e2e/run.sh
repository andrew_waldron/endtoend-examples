#!/bin/bash

function pause() {
   read -p "$*"
}

WORKING_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TEST_SUITE=smoke

if ! [ -z $1 ]
then
  TEST_SUITE=$1
fi

HIPTEST_CONFIG_PATH=${WORKING_DIR}/hiptest/${TEST_SUITE}
hiptest-publisher \
  --config-file=${HIPTEST_CONFIG_PATH}/hiptest-publisher.conf \
  --output-directory=${HIPTEST_CONFIG_PATH}

# yes, I feel bad about myself. But I harcoded this never the less...
INSERT_LINE="var Actionwords = require(\"../automated/actionwords\");"
FILE_TO_UPDATE=${WORKING_DIR}/hiptest/${TEST_SUITE}/project_test.js
echo -e "${INSERT_LINE}\n\n$(cat ${FILE_TO_UPDATE})" > ${FILE_TO_UPDATE}

# run the tests
protractor ${WORKING_DIR}/protractor.conf.js --suite ${TEST_SUITE}

mkdir -p ${HIPTEST_CONFIG_PATH}/testresults/
mv ${WORKING_DIR}/hiptest/automated/testresults/xmloutput.xml ${HIPTEST_CONFIG_PATH}/testresults/xmloutput.xml

# publish results
hiptest-publisher \
  --config-file=${HIPTEST_CONFIG_PATH}/hiptest-publisher.conf \
  --push=${HIPTEST_CONFIG_PATH}/testresults/xmloutput.xml \
  --push-format=junit

echo "All done peeps"