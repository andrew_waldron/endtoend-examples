module.exports = {
  menu: element(by.id('menu')),
  peopleMenu: element(by.id('peopleMenu')),
  teamsMenu: element(by.id('teamsMenu')),
  productsMenu: element(by.id('productsMenu')),
  logoutButton: element(by.id('logoutMenu'))
};