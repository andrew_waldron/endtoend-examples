module.exports = {
  usernameInput: element(by.id('usernameInput')),
  passwordInput: element(by.id('passwordInput')),
  loginButton: element(by.id('loginButton'))
};