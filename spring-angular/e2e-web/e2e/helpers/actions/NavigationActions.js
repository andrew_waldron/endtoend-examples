'use strict';

var navigation = require('../pages/navigation');
var matchers = require('jasmine-expect');

function checkForAdminScreen() {
  expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + navigation.adminURL);
}

function goToHomepage() {
  browser.get(navigation.homepageURL);
}

function checkForHomepage() {
  expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + navigation.homepageURL);
}

function checkForLoginPage() {
  expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + navigation.loginURL);
}

module.exports = {
  checkForHomepage: checkForHomepage,
  checkForAdminScreen: checkForAdminScreen,
  checkForLoginPage: checkForLoginPage,
  goToHomepage: goToHomepage
};