'use strict';

var homeMenuControls = require('../pages/home');

function openMenu() {
  homeMenuControls.menu.click();
}

function selectLoginButton() {
  homeMenuControls.login.click();
}

module.exports = {
  openMenu: openMenu,
  selectLoginButton: selectLoginButton
};