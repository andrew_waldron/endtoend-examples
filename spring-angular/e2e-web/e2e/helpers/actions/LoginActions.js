'use strict';

var loginPage = require('../pages/login');

function login(username, password) {
  loginPage.usernameInput.sendKeys(username);
  loginPage.passwordInput.sendKeys(password);
  loginPage.loginButton.click();
}

module.exports = {
  login: login
};
