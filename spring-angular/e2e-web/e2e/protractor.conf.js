'use strict';

var jasmineReporters = require('jasmine-reporters');

exports.config = {
    allScriptsTimeout: 11000,
    capabilities: {
        'browserName': 'chrome'
    },
    framework: 'jasmine',
    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    },
    baseUrl: 'http://localhost:8090/index.htm',
    suites: {
        smoke: 'hiptest/smoke/project_test.js',
        regression: 'hiptest/regression/project_test.js'
    },
    framework: 'jasmine2',
    onPrepare: function() {
        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            savePath: 'e2e/hiptest/automated/testresults',
            filePrefix: 'xmloutput'
        }));
    }

};