'use strict';

function squadService($resource, apiBaseUrl) {
  var baseResourceUrl = apiBaseUrl + 'squads/:squadId';
  var paramDefaults = {};
  var config = {};
  var customActions = {
    getAll: {
      method: 'GET',
      url: apiBaseUrl + 'squads',
      isArray: true
    }
  };

  return $resource(baseResourceUrl, paramDefaults, customActions, config);
}

angular.module('fusewall.services')
  .factory('squadService', ['$resource', 'apiBaseUrl', squadService]);