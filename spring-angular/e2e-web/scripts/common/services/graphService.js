'use strict';

function graphService($resource, apiBaseUrl) {
  var baseResourceUrl = apiBaseUrl + 'd3graph';
  var paramDefaults = {};
  var config = {};
  var customActions = {
    getNodeDetails: {
      method: 'GET',
      url: apiBaseUrl + 'd3graph/:entityId',
      isArray: false
    }
  };

  return $resource(baseResourceUrl, paramDefaults, customActions, config);
}

angular.module('fusewall.services')
  .factory('graphService', ['$resource', 'apiBaseUrl', graphService]);