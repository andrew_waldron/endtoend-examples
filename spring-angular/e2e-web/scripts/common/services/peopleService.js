'use strict';

function peopleService($resource, apiBaseUrl) {
  var baseResourceUrl = apiBaseUrl + 'persons/:personId';
  var paramDefaults = {};
  var config = {};
  var customActions = {
    getAll: {
      method: 'GET',
      url: apiBaseUrl + 'persons',
      isArray: true
    },
    getSquads: {
      method: 'GET',
      url: apiBaseUrl + 'persons/:personId/squads/',
      isArray: true
    },
    deleteAllSquads: {
      method: 'DELETE',
      url: apiBaseUrl + 'persons/:personId/squads/'
    },
    addSquad: {
      method: 'POST',
      url: apiBaseUrl + 'persons/:personId/squads/:squadId'
    }
  };

  return $resource(baseResourceUrl, paramDefaults, customActions, config);
}

angular.module('fusewall.services')
  .factory('peopleService', ['$resource', 'apiBaseUrl', peopleService]);