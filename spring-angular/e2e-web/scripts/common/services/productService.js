'use strict';

function productService($resource, apiBaseUrl) {
  var baseResourceUrl = apiBaseUrl + 'products/:productId';
  var paramDefaults = {};
  var config = {};
  var customActions = {
    getAll: {
      method: 'GET',
      url: apiBaseUrl + 'products',
      isArray: true
    },
    getSquads: {
      method: 'GET',
      url: apiBaseUrl + 'products/:productId/squads/',
      isArray: true
    },
    deleteAllSquads: {
      method: 'DELETE',
      url: apiBaseUrl + 'products/:productId/squads/'
    },
    addSquad: {
      method: 'POST',
      url: apiBaseUrl + 'products/:productId/squads/:squadId',
      isArray: false
    }
  };

  return $resource(baseResourceUrl, paramDefaults, customActions, config);
}

angular.module('fusewall.services')
  .factory('productService', ['$resource', 'apiBaseUrl', productService]);