'use strict';

function loginController($scope, loginService) {
  var vm = $scope;

  (function initialize() {
    vm.adminLogin = adminLogin;
    vm.user = {};
  })();

  function adminLogin() {
    loginService.attemptLogin(vm.user.name, vm.user.password);
  }
}

function loginControllerConfig($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'scripts/login/partials/login.htm?bustCache=true',
    controller: 'LoginController'
  });
}

angular.module('fusewall.controllers')
  .config(['$routeProvider', loginControllerConfig])
  .controller('LoginController', ['$scope', 'loginService', loginController]);
