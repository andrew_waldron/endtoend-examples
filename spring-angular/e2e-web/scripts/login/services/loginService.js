function loginService($http, apiBaseUrl, $location, $cookies) {
  var authCookieKey = 'authorizationHeader';

  function forceLogin(user, pass) {
    setAuthHeaders(getBasicAuth(user, pass));
  }

  function attemptLogin(user, pass) {
    setAuthHeaders(getBasicAuth(user, pass));

    return $http.get(apiBaseUrl, {withCredentials: true})
      .then(navigateToAdminPage, clearAuthHeaders);
  }

  function setAuthHeaders(authorizationHeader) {
    $http.defaults.headers.common.Authorization = authorizationHeader;
    $cookies.put(authCookieKey, authorizationHeader);
  }

  function initialize() {
    var savedHeader = $cookies.get(authCookieKey);
    if (savedHeader) {
      setAuthHeaders(savedHeader);
    }
  }

  function getBasicAuth(user, pass) {
    return "Basic " + btoa(user + ":" + pass);
  }

  function clearAuthHeaders() {
    delete $http.defaults.headers.common.Authorization;
    $cookies.remove(authCookieKey);
  }

  function navigateToAdminPage() {
    $location.path("/admin");
  }

  function logout() {
    clearAuthHeaders();
    $location.path("/");
  }

  return {
    forceLogin: forceLogin,
    attemptLogin: attemptLogin,
    logout: logout,
    initialize: initialize
  };
}

angular.module('fusewall.services')
  .factory('loginService', ['$http', 'apiBaseUrl', '$location', '$cookies', loginService]);