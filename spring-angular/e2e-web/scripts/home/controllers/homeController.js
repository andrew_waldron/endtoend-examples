'use strict';

function homeController($scope, $mdDialog, apiBaseUrl, graphService) {
  var vm = $scope;
  var width = window.innerWidth;
  var height = window.innerHeight;
  var radius = 24;
  var svg = null;
  var spinner = null;
  var force = null;
  var isZoomDisabled = false;
  var currentScale = 0;
  var numberAtTheScale = [0, 0, 0, 0, 0, 0, 0];
  var numberAllowedAtEachScale = [3, 5, 5, 7, 22, 30, 0];
  var translateX = 0;
  var translateY = 0;
  var lastTouchLocation = {x: 0, y: 0};

  var sizeFactors = {
    "org": 3,
    "product": 1.4,
    "team": 2,
    "people": 1,
    "orgSelected": 9,
    "productSelected": 4.2,
    "teamSelected": 6,
    "peopleSelected": 3
  };

  (function initialize() {
    vm.currentSearch = '';
    vm.isOpen = false;
    vm.$watch('currentSearch', processSearchChange);

    loadAndRenderPeopleWall();
  })();

  function processSearchChange() {
    if (!svg) return;

    getAllNodes()
      .select("circle")
      .attr("x", getSymmetricOffset)
      .attr("y", getSymmetricOffset)
      .attr("r", getSymmetricNodeRadius)
      .attr("stroke", getSearchColorEl)
      .style("opacity", getNodeSearchOpacity);

    getAllNodes().select("rect")
      .attr("x", getSymmetricOffset)
      .attr("y", getSymmetricOffset)
      .attr("width", getSymmetricNodeSize)
      .attr("height", getSymmetricNodeSize);

    getAllNodes().select("image")
      .attr("x", getSymmetricOffset)
      .attr("y", getSymmetricOffset)
      .attr("width", getSymmetricNodeSize)
      .attr("height", getSymmetricNodeSize)
      .style("opacity", getNodeSearchOpacity);

    getAllNodes()
      .attr("class", getSelectedClass);

    getAllNodes()
      .attr("clip-path", getPathTypeIdAsUrl);

    if (getAllSelectedNodes().size() < 10) {
      getAllSelectedNodes().moveToFront();
    }
  }

  function getSelectedClass(node) {
    return !vm.currentSearch || matchesSearchCriteria(node) ? "node selected" : "node";
  }

  function getNodeSearchOpacity(node) {
    return !vm.currentSearch || matchesSearchCriteria(node) ? 1 : 0.2;
  }

  function getSearchColorEl(node) {
    return matchesSearchCriteria(node) ? "red" : "#777";
  }

  function matchesSearchCriteria(node) {
    return Boolean(vm.currentSearch && node && node.title && node.title.match(new RegExp(vm.currentSearch, "i")));
  }

  function hasMultipleTeams(d3VersionOfLinks, node, index) {
    if (node.nodeType !== "people") {
      return false;
    }

    return _.filter(d3VersionOfLinks, _.partial(isLinkAimedAtIndex, index)).length > 1;
  }

  function updateLinksToSimplifyDiagram(d3VersionOfLinks, rawNodes) {
    var peopleWithMultipleTeams = _.filter(rawNodes, _.partial(hasMultipleTeams, d3VersionOfLinks));

    _.each(peopleWithMultipleTeams, function (person) {
      d3VersionOfLinks = updateLinksToMapFromCenterToPerson(d3VersionOfLinks, rawNodes.indexOf(person));
    });

    return d3VersionOfLinks;
  }

  function isLinkAimedAtIndex(index, link) {
    return link.target === index;
  }

  function isLinkThatIsNotAimedAtAnyOfTheseIndexes(indexArray, link) {
    return !_.any(indexArray, function (index) {
      return link.target === index;
    });
  }

  function updateLinksToMapFromCenterToPerson(links, personIndex) {
    var teamsThisPersonIsConnectedTo = _.map(_.filter(links, _.partial(isLinkAimedAtIndex, personIndex)), function (link) {
      return link.source;
    });

    var linksWithoutTeamsConnectedToCenter = _.filter(links, _.partial(isLinkThatIsNotAimedAtAnyOfTheseIndexes, teamsThisPersonIsConnectedTo));
    var linksWithTeamsReconnectedToPerson = addLinksToPerson(linksWithoutTeamsConnectedToCenter, personIndex, teamsThisPersonIsConnectedTo);

    linksWithTeamsReconnectedToPerson.push({
      source: getOrgIndex(links),
      target: personIndex,
      sourceType: 'org'
    });

    return linksWithTeamsReconnectedToPerson;
  }

  function addLinksToPerson(links, personIndex, teamIndexes) {
    var newLinksToAdd = _.map(teamIndexes, function (teamIndex) {
      return {
        source: personIndex,
        target: teamIndex,
        sourceType: 'team'
      };
    });

    return [].concat(links, newLinksToAdd);
  }

  function getOrgIndex(links) {
    return _.find(links, function (link) {
      return link.sourceType === 'org';
    }).source;
  }

  function createTheD3Graph(error, json) {
    addCustomZOrderFunctions();

    svg = giveMeACanvas();
    force = getAStandardForceLayout();
    if (error) throw error;

    spinner.stop();

    makeClickSmarter();

    var d3VersionOfLinks = convertIdsToIndexes(json.links, json.nodes);
    d3VersionOfLinks = updateLinksToSimplifyDiagram(d3VersionOfLinks, json.nodes);

    createClippingPath();
    createForceLayout(json, d3VersionOfLinks);
    createAllLinks(d3VersionOfLinks);
    createAllNodes(json.nodes);
    fillBackgroundsWhite();
    displayAllImages();
    drawBorders();

    helpTheZoomPanOut();
  }

  function addCustomZOrderFunctions() {
    d3.selection.prototype.moveToFront = function () {
      return this.each(function () {
        this.parentNode.appendChild(this);
      });
    };
  }

  function createForceLayout(json, d3VersionOfLinks) {
    force
      .nodes(json.nodes)
      .links(d3VersionOfLinks)
      .on("tick", ensureNodesStayInBounds)
      .start();
  }

  function makeClickSmarter() {
    d3.select(".fusewall-demo")
      .on("touchstart", smarterClick)
      .on("touchmove", smarterClick);
  }

  function smarterClick() {
    d3.event.preventDefault();
  }

  function drawBorders() {
    getAllNodes().append("circle")
      .attr("x", getSymmetricOffset)
      .attr("y", getSymmetricOffset)
      .attr("r", getSymmetricNodeRadius)
      .attr("stroke", getSearchColorEl)
      .attr("stroke-width", "4")
      .attr('fill', "none");
  }

  function createAllLinks(d3VersionOfLinks) {
    getAllLinks()
      .data(d3VersionOfLinks)
      .enter().append("line")
      .attr("class", "link");
  }

  function createAllNodes(nodes) {
    getAllNodes()
      .data(nodes)
      .enter().append("g")
      .attr("class", "node")
      .attr("clip-path", getPathTypeIdAsUrl)
      .call(force.drag);
  }

  function displayAllImages() {
    getAllNodes()
      .append("image")
      .attr("id", getUniqueIdForNodeThatBeginsWithALetterToMakeD3Happy)
      .attr("xlink:href", getImageFromNode)
      .attr("x", getSymmetricOffset)
      .attr("y", getSymmetricOffset)
      .attr("width", getSymmetricNodeSize)
      .attr("height", getSymmetricNodeSize)
      .attr("class", getNodeClass)
      .on('click', selectNode);
  }

  function fillBackgroundsWhite() {
    getAllNodes().append("rect")
      .attr("x", getSymmetricOffset)
      .attr("y", getSymmetricOffset)
      .attr("width", getSymmetricNodeSize)
      .attr("height", getSymmetricNodeSize)
      .attr("fill", "#FFF");
  }

  function createClippingPath() {
    var pathTypes = ['people', 'product', 'org', 'team', 'peopleSelected', 'productSelected', 'orgSelected', 'teamSelected'];

    svg.append('defs')
      .selectAll('clipPath')
      .data(pathTypes)
      .enter()
      .append("clipPath")
      .attr("id", getPathTypeId)

      .append("circle")
      .attr("fill", "#FFFFFF")
      .attr("x", getSymmetricOffset)
      .attr("y", getSymmetricOffset)
      .attr("r", getSymmetricNodeRadius);
  }

  function getPathTypeIdAsUrl(node) {
    var selectedExtras = matchesSearchCriteria(node) ? 'Selected' : '';
    return 'url(#clipPath' + node.nodeType + selectedExtras + ")";
  }

  function getPathTypeId(pathType) {
    return 'clipPath' + pathType;
  }

  function getNodeClass(node) {
    return node.nodeType;
  }

  function getSymmetricNodeRadius(node) {
    return radius * getSizeFactor(node);
  }

  function getSymmetricNodeSize(node) {
    return radius * getSizeFactor(node) * 2;
  }

  function getSymmetricOffset(node) {
    return -radius * getSizeFactor(node);
  }

  function getSizeFactor(node) {
    var additionalSizeFactor = matchesSearchCriteria(node) ? 3 : 1;
    return sizeFactors[node.nodeType || node.sourceType || node] * additionalSizeFactor;
  }

  function getImageFromNode(node) {
    if (!node.imageUrl) {
      queryImageAndUpdateNode(node);
      return '';
    }

    return node.imageUrl;
  }

  function getUniqueIdForNodeThatBeginsWithALetterToMakeD3Happy(node) {
    return 'N' + node.id;
  }

  function queryImageAndUpdateNode(node) {
    graphService.getNodeDetails({entityId: node.id}).$promise.then(function (resultNode) {
      node.imageUrl = resultNode.imageUrl;
      svg.select("#N" + node.id).attr("xlink:href", resultNode.imageUrl);
    });
  }

  function giveMeACanvas() {
    return d3.select(".fusewall-demo")
      .append("svg")
      .attr("width", "100%")
      .attr("height", "100%")
      .on('touchstart', handleTouchStart)
      .call(d3.behavior.zoom().on("zoom", processZoomAndPan))
      .on("dblclick.zoom", null)
      .append("g");
  }

  function handleTouchStart() {
    var touchStartDetails = d3.event.targetTouches[0];
    lastTouchLocation.x = touchStartDetails.clientX;
    lastTouchLocation.y = touchStartDetails.clientY;
  }

  function processZoomAndPan() {
    if (!isZoomDisabled) {
      var offsetX, offsetY;

      if (d3.event.sourceEvent.type === "mousemove") {
        offsetX = d3.event.sourceEvent.movementX;
        offsetY = d3.event.sourceEvent.movementY;
      }

      if ((d3.event.sourceEvent.type === "touchmove") &&
        d3.event.sourceEvent.targetTouches.length > 0) {
        var touchDetails = d3.event.sourceEvent.targetTouches[0];
        offsetX = touchDetails.clientX - lastTouchLocation.x;
        offsetY = touchDetails.clientY - lastTouchLocation.y;

        lastTouchLocation.x = touchDetails.clientX;
        lastTouchLocation.y = touchDetails.clientY;
      }

      if (offsetX) translateX += offsetX;
      if (offsetY) translateY += offsetY;

      svg.attr("transform", "translate(" + translateX + "," + translateY + ")" + " scale(" + d3.event.scale + ")");
    }
  }

  function helpTheZoomPanOut() {
    getAllNodes()
      .on("mousedown", disableZoom)
      .on("touchstart", disableZoom)
      .on("mouseup", enableZoom)
      .on("touchend", enableZoom);
  }

  function disableZoom(node) {
    if (node.nodeType !== "org") {
      isZoomDisabled = true;
    }
  }

  function enableZoom() {
    isZoomDisabled = false;
  }

  function getIndexOf(id, nodesArray) {
    return _.findIndex(nodesArray, {id: id});
  }

  function getAStandardForceLayout() {
    return d3.layout.force()
      .gravity(0)
      .distance(getLinkLength)
      .charge(-200)
      .friction(0.96)
      .size([width, height]);
  }

  function logScaleFactor(node) {
    return Math.log(getSizeFactor(node)) + 1;
  }

  function getLinkLength(node) {
    var randomOffset = 0;
    if (node.sourceType === 'org') {
      while (numberAtTheScale[currentScale] >= numberAllowedAtEachScale[currentScale]) {
        currentScale++;
      }

      numberAtTheScale[currentScale]++;
      randomOffset = currentScale;
    }
    return 100 * (logScaleFactor(node) + randomOffset);
  }

  function convertIdsToIndexes(idsArray, nodesArray) {
    return _.map(idsArray, function (idArrayElement) {
      var mapped = {
        source: getIndexOf(idArrayElement.source, nodesArray),
        target: getIndexOf(idArrayElement.target, nodesArray),
        sourceType: idArrayElement.sourceType
      };

      mapped.id = nodesArray[mapped.source].id;
      return mapped;
    })
  }

  function loadAndRenderPeopleWall() {
    spinner = createSpinner('fusewall-demo');

    d3.json(apiBaseUrl + "d3graph", createTheD3Graph);
  }

  function getAllSelectedNodes() {
    return svg.selectAll(".node.selected");
  }

  function getAllNodes() {
    return svg.selectAll(".node");
  }

  function getAllLinks() {
    return svg.selectAll(".link");
  }

  function ensureNodesStayInBounds() {
    getAllNodes()
      .attr("cx", function (d) {
        var nodeRadius = -getSymmetricOffset(d);
        if (d.nodeType == "org")  return d.x = width / 2.0 - nodeRadius;
        return d.x;
      })
      .attr("cy", function (d) {
        var nodeRadius = -getSymmetricOffset(d);
        if (d.nodeType == "org")  return d.y = height / 2.0 - nodeRadius;
        return d.y;
      })
      .attr("transform", function (d) {
        return "translate(" + d.x + "," + d.y + ")";
      });

    getAllLinks().attr("x1", function (d) {
      return d.source.x;
    })
      .attr("y1", function (d) {
        return d.source.y;
      })
      .attr("x2", function (d) {
        return d.target.x;
      })
      .attr("y2", function (d) {
        return d.target.y;
      });
  }

  function selectNode(node) {
    if (d3.event.defaultPrevented) {
      return;
    }

    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'scripts/home/partials/detailsDialog.htm?bustCache=true',
      parent: angular.element(document.body),
      clickOutsideToClose: true
    });

    function DialogController($scope, $mdDialog) {
      $scope.entity = node;
      $scope.cancel = $mdDialog.cancel;
    }
  }

  function createSpinner(target) {
    var opts = {lines: 13, length: 28, width: 14, radius: 44, scale: 1.25, corners: 0.7, color: '#000', opacity: 0.05, rotate: 0, direction: 1, speed: 1, trail: 60, fps: 20, zIndex: 2e9, className: 'spinner', top: '50%', left: '50%', shadow: false, hwaccel: false, position: 'absolute'};
    return new Spinner(opts).spin(document.getElementById(target));
  }
}

function homeControllerConfig($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'scripts/home/partials/home.htm?bustCache=true',
    controller: 'HomeController'
  });
}

angular.module('fusewall.controllers')
  .config(['$routeProvider', homeControllerConfig])
  .controller('HomeController', ['$scope', '$mdDialog', 'apiBaseUrl', 'graphService', homeController]);
