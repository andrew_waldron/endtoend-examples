'use strict';

function adminController($scope, screenConfigs, peopleService, squadService, productService, loginService, _, $q, $location, appBaseUrl) {
  var vm = $scope;
  var screenResources, mappingMethods;

  (function initialize() {
    vm.isOpen = false;
    vm.logout = logout;
    vm.selectThingToEdit = selectThingToEdit;
    vm.saveEdit = saveEdit;
    vm.cancelEdit = cancelEdit;
    vm.editTheEntity = editTheEntity;
    vm.deleteSelected = deleteSelected;
    vm.addSquad = addSquad;
    vm.removeSquad = removeSquad;
    vm.selected = {};

    setupScreenResources();
    setupMappingMethods();
    selectThingToEdit($location.search().view || 'people');
  })();

  function setupScreenResources() {
    screenResources = {
      people: peopleService,
      squads: squadService,
      products: productService
    };
  }

  function setupMappingMethods() {
    mappingMethods = {
      people: mapDTOPersonToEditPersonAndLoadSquads,
      squads: mapDTOSquadToEditSquad,
      products: mapDTOProductToEditProductAndLoadSquads
    }
  }

  function selectThingToEdit(newConfig) {
    $location.search('view', newConfig);

    vm.currentResource = screenResources[newConfig];
    vm.mapMethod = mappingMethods[newConfig];
    vm.currentEditConfiguration = screenConfigs[newConfig];
    vm.configurationAsString = newConfig;

    refreshTheScreen();
  }

  function loadEditingList() {
    vm.currentResource.getAll().$promise.then(function (data) {
      vm.allTheThingsList = data;

      refreshAllSquadsList();
    });
  }

  function refreshAllSquadsList() {
    if (vm.configurationAsString === 'squads') {
      return;
    }

    squadService.query().$promise.then(function (data) {
      vm.allSquads = data;
    });
  }

  function deleteSelected() {
    if (!vm.originalEditingEntity) {
      return;
    }

    vm.currentResource.delete(createIdFilter(vm.originalEditingEntity)).$promise.then(refreshTheScreen);
  }

  function saveEdit() {
    getSaveEntity().then(actuallySave)
  }

  function actuallySave(savingEntity) {
    var entityWithValidImage = applySaveAsUpdatesToOriginal(savingEntity);
    var idFilter = createIdFilter(entityWithValidImage);
    vm.currentResource.save(idFilter, entityWithValidImage).$promise.then(saveOtherEntityDetails);
  }

  function createIdFilter(entity) {
    var entityId = getIdFieldName(entity);
    var idFilter = {};
    idFilter[entityId] = entity[entityId];
    return idFilter;
  }

  function getIdFieldName(entity) {
    return _.find(_.keys(entity), function (key) {
      return endsWith(key, "Id") && key.indexOf('shareable') < 0;
    });
  }

  function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  }

  function applySaveAsUpdatesToOriginal(savingEntity) {
    if (!vm.originalEditingEntity || !vm.originalEditingEntity.title) {
      return savingEntity;
    }

    vm.originalEditingEntity.title = savingEntity.title;
    vm.originalEditingEntity.imageUrl = savingEntity.imageUrl || vm.originalEditingEntity.imageUrl;
    vm.originalEditingEntity.description = savingEntity.description;
    return vm.originalEditingEntity;
  }

  function saveOtherEntityDetails(savedEntity) {
    var saveSquadsMethod = null;

    if (vm.configurationAsString === 'people') {
      saveSquadsMethod = saveSquadsAssociatedWithThisPerson;
    } else if (vm.configurationAsString === 'products') {
      saveSquadsMethod = saveSquadsAssociatedWithThisProduct;
    }

    if (saveSquadsMethod) {
      saveSquadsMethod(savedEntity).then(refreshTheScreen);
    } else {
      refreshTheScreen();
    }
  }

  function saveSquadsAssociatedWithThisProduct(savedProduct) {
    var deferred = $q.defer();

    productService.deleteAllSquads({productId: savedProduct.productId}).$promise.then(function () {
      vm.selected.squads.forEach(function (squad) {
        productService.addSquad({
          productId: savedProduct.productId,
          squadId: squad.squadId
        }, {});
      });

      deferred.resolve(savedProduct);
    });

    return deferred.promise;
  }

  function saveSquadsAssociatedWithThisPerson(savedPerson) {
    var deferred = $q.defer();

    peopleService.deleteAllSquads({personId: savedPerson.personId}).$promise.then(function () {
      vm.selected.squads.forEach(function (squad) {
        peopleService.addSquad({
          personId: savedPerson.personId,
          squadId: squad.squadId
        }, {});
      });

      deferred.resolve(savedPerson);
    });

    return deferred.promise;
  }

  function refreshTheScreen() {
    loadEditingList();
    cancelEdit();
  }

  function cancelEdit() {
    vm.files = [];
    vm.originalEditingEntity = null;
    vm.selected = {};
  }

  function editTheEntity(entity) {
    vm.originalEditingEntity = entity;

    vm.mapMethod(entity).then(function (editingPerson) {
      vm.selected = editingPerson;
    });
  }

  function addSquad() {
    vm.selected.squads = vm.selected.squads || [];
    vm.selected.squads.push(getEmptySquad());
  }

  function removeSquad() {
    if (vm.selected.squads) {
      vm.selected.squads.pop();
    }
  }

  function logout() {
    loginService.logout();
  }

  function mapDTOPersonToEditPersonAndLoadSquads(dtoPerson) {
    var deferred = $q.defer();

    getSquadsRelatedToAPerson(dtoPerson).$promise.then(function (squadList) {
      deferred.resolve(mapDTOToEditObject(dtoPerson, squadList));
    });

    return deferred.promise;
  }

  function mapDTOSquadToEditSquad(dtoSquad) {
    var deferred = $q.defer();
    deferred.resolve(mapDTOToEditObject(dtoSquad));
    return deferred.promise;
  }

  function mapDTOProductToEditProductAndLoadSquads(dtoProduct) {
    var deferred = $q.defer();

    getSquadsRelatedToAProduct(dtoProduct).$promise.then(function (squadList) {
      deferred.resolve(mapDTOToEditObject(dtoProduct, squadList));
    });

    return deferred.promise;
  }

  function getSquadsRelatedToAProduct(dtoProduct) {
    return productService.getSquads({productId: dtoProduct.productId});
  }

  function mapDTOToEditObject(entity, squads) {

    var mapped = {
      name: entity.title,
      imageUrl: entity.imageUrl,
      details: entity.description,
      singleEditLink: appBaseUrl + '#!/person/' + entity.shareableId
    };

    if (squads) {
      mapped.squads = squads;
    }

    return mapped;
  }

  function getEmptySquad() {
    return {};
  }

  function getSquadsRelatedToAPerson(dtoPerson) {
    return peopleService.getSquads({personId: dtoPerson.personId});
  }

  function getSaveEntity() {
    var deferred = $q.defer();

    getSelectedFileAsBase64().then(function (fileAsBase64) {
      deferred.resolve({
        title: vm.selected.name,
        imageUrl: fileAsBase64,
        description: vm.selected.details
      });
    });

    return deferred.promise;
  }

  function getSelectedFileAsBase64() {
    var deferred = $q.defer();

    if (vm.files && vm.files.length >= 1) {
      var fileReader = new FileReader();
      fileReader.onload = function (data) {
        deferred.resolve(data.target.result);
      };

      fileReader.readAsDataURL(vm.files[0].lfFile);
    } else {
      deferred.resolve(null);
    }

    return deferred.promise;
  }
}

function adminControllerConfig($routeProvider) {
  $routeProvider.when('/admin', {
    templateUrl: 'scripts/admin/partials/admin.htm?bustCache=true',
    controller: 'AdminController'
  });
}

angular.module('fusewall.controllers')
  .config(['$routeProvider', adminControllerConfig])
  .controller('AdminController', ['$scope', 'editScreenConfigurations', 'peopleService', 'squadService', 'productService', 'loginService', '_', '$q', '$location', 'appBaseUrl', adminController]);
