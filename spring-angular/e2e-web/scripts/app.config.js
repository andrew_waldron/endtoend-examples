angular.module("fusewall.config", [])
.constant("apiBaseUrl", "http://localhost:8080/")
.constant("appBaseUrl", "http://localhost:8090/")
.constant("version", "$VERSION")
.constant("editScreenConfigurations", {"people":{"title":"Fuse People","display":"people","thingy":"Person","detailsLabel":"Ask Me About"},"squads":{"title":"Fuse Squads","display":"squads","thingy":"Squadron","detailsLabel":"About The Team"},"products":{"title":"Fuse Products","display":"products","thingy":"Product","detailsLabel":"Product Details"}});
