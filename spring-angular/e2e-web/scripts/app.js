'use strict';

function UnderscoreFactory($window) {
  return $window._;
}

angular.module('underscore', []).factory('_', ['$window', UnderscoreFactory]);

function rootController(loginService) {
  loginService.initialize();
}

angular
  .module('fusewall', ['fusewall.config', 'ngRoute', 'fusewall.controllers', 'fusewall.services', 'ngMaterial', 'underscore', 'ngMdIcons', 'ngResource', 'spring-data-rest', 'ngCookies', 'lfNgMdFileInput', 'ngSanitize', 'ng-showdown'])
  .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.otherwise({redirectTo: '/home'});
  }])
  .run(['loginService', rootController]);


angular.module('fusewall.controllers', ['fusewall.config', 'ngRoute', 'ngResource', 'spring-data-rest', 'ngMaterial', 'lfNgMdFileInput']);
angular.module('fusewall.services', ['fusewall.config', 'ngRoute', 'ngResource', 'spring-data-rest', 'ngCookies', 'lfNgMdFileInput']);
