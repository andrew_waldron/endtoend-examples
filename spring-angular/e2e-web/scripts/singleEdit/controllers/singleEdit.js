'use strict';

function singleEditController($scope, $routeParams, peopleService, loginService, _, $q, squadService, $mdToast) {
  var vm = $scope;

  (function initialize() {
    vm.isOpen = false;
    vm.logout = loginService.logout;
    vm.saveEdit = saveEdit;
    vm.addSquad = addSquad;
    vm.removeSquad = removeSquad;
    vm.selected = {};

    loginService.forceLogin('uuidEdit', 'uuidEdit');
    loadSquadsList().then(function () {
      loadThePerson($routeParams.personId);
    });
  })();

  function loadSquadsList() {
    var deferred = $q.defer();

    squadService.query().$promise.then(function (data) {
      vm.allSquads = data;
      deferred.resolve(data);
    });

    return deferred.promise;
  }

  function loadThePerson(shareableId) {
    peopleService.get({personId: shareableId, isShareableId: true}).$promise.then(function (loadedPerson) {
      vm.originalEditingEntity = loadedPerson;

      mapDTOPersonToEditPersonAndLoadSquads(loadedPerson).then(function (editingPerson) {
        vm.selected = editingPerson;
      });
    });
  }

  function addSquad() {
    vm.selected.squads = vm.selected.squads || [];
    vm.selected.squads.push(getEmptySquad());
  }

  function getEmptySquad() {
    return {};
  }

  function removeSquad() {
    if (vm.selected.squads) {
      vm.selected.squads.pop();
    }
  }

  function mapDTOPersonToEditPersonAndLoadSquads(dtoPerson) {
    var deferred = $q.defer();

    getSquadsRelatedToAPerson(dtoPerson).$promise.then(function (squadList) {
      deferred.resolve(mapDTOToEditObject(dtoPerson, squadList));
    });

    return deferred.promise;
  }

  function mapDTOToEditObject(entity, squads) {
    var mapped = {
      name: entity.title,
      imageUrl: entity.imageUrl,
      details: entity.description
    };

    if (squads) {
      mapped.squads = squads;
    }

    return mapped;
  }

  function getSquadsRelatedToAPerson(dtoPerson) {
    return peopleService.getSquads({personId: dtoPerson.shareableId, isShareableId: true});
  }

  function saveEdit() {
    getSaveEntity().then(actuallySave)
  }

  function getSaveEntity() {
    var deferred = $q.defer();

    getSelectedFileAsBase64().then(function (fileAsBase64) {
      deferred.resolve({
        title: vm.selected.name,
        imageUrl: fileAsBase64,
        description: vm.selected.details
      });
    });

    return deferred.promise;
  }

  function getSelectedFileAsBase64() {
    var deferred = $q.defer();

    if (vm.files && vm.files.length >= 1) {
      var fileReader = new FileReader();
      fileReader.onload = function (data) {
        deferred.resolve(data.target.result);
      };

      fileReader.readAsDataURL(vm.files[0].lfFile);
    } else {
      deferred.resolve(null);
    }

    return deferred.promise;
  }

  function actuallySave(savingEntity) {
    var entityWithValidImage = applySaveAsUpdatesToOriginal(savingEntity);
    var idFilter = {personId: entityWithValidImage.shareableId, isShareableId: true};
    peopleService.save(idFilter, entityWithValidImage).$promise.then(function (savedPerson) {
      saveSquadsAssociatedWithThisPerson(savedPerson).then(function () {
        $mdToast.show(
          $mdToast.simple()
            .theme("success-toast")
            .textContent('Save Completed!')
            .position('top right')
            .hideDelay(3000)
        );
      });
    });
  }

  function createIdFilter(entity) {
    var entityId = getIdFieldName(entity);
    var idFilter = {};
    idFilter[entityId] = entity[entityId];
    return idFilter;
  }

  function getIdFieldName(entity) {
    return _.find(_.keys(entity), function (key) {
      return endsWith(key, "Id") && key.indexOf('shareable') < 0;
    });
  }

  function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  }

  function applySaveAsUpdatesToOriginal(savingEntity) {
    if (!vm.originalEditingEntity || !vm.originalEditingEntity.title) {
      return savingEntity;
    }

    vm.originalEditingEntity.title = savingEntity.title;
    vm.originalEditingEntity.imageUrl = savingEntity.imageUrl || vm.originalEditingEntity.imageUrl;
    vm.originalEditingEntity.description = savingEntity.description;
    return vm.originalEditingEntity;
  }

  function saveSquadsAssociatedWithThisPerson(savedPerson) {
    var deferred = $q.defer();

    peopleService.deleteAllSquads({personId: savedPerson.shareableId, isShareableId: true}).$promise.then(function () {
      var addSquadPromises = [];

      vm.selected.squads.forEach(function (squad) {
        addSquadPromises.push(peopleService.addSquad({
          personId: savedPerson.shareableId,
          squadId: squad.squadId,
          isShareableId: true
        }, {}));
      });

      if (addSquadPromises.length > 0) {
        $q.all(addSquadPromises).then(function () {
          deferred.resolve(savedPerson);
        });
      } else {
        deferred.resolve(savedPerson);
      }
    });

    return deferred.promise;
  }
}

function singleEditControllerConfig($routeProvider) {
  $routeProvider.when('/person/:personId', {
    templateUrl: 'scripts/singleEdit/partials/singleEdit.htm?bustCache=true',
    controller: 'SingleEditController'
  });
}

angular.module('fusewall.controllers')
  .config(['$routeProvider', singleEditControllerConfig])
  .controller('SingleEditController', ['$scope', '$routeParams', 'peopleService', 'loginService', '_', '$q', 'squadService', '$mdToast', singleEditController]);
