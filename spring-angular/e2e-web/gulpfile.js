var gulp = require('gulp');
var gulpNgConfig = require('gulp-ng-config');
var rename = require('gulp-rename');
var rimraf = require('gulp-rimraf');
var clean = require('gulp-clean');
var _ = require('underscore');
var replace = require('gulp-string-replace');
var filter = require('gulp-filter');
var cacheBustSearchString = '\\?bustCache=true';
var cacheBustReplaceString = '?bustCache=' + new Date().getTime();

var buildPath = '../dist/';
var includedExternalFiles = [
  'node_modules/angular/angular.min.js',
  'node_modules/angular-route/angular-route.min.js',
  'node_modules/angular-animate/angular-animate.min.js',
  'node_modules/angular-aria/angular-aria.min.js',
  'node_modules/angular-resource/angular-resource.min.js',
  'node_modules/angular-material/angular-material.min.js',
  'node_modules/angular-material/angular-material.min.css',
  'node_modules/angular-cookies/angular-cookies.min.js',
  'node_modules/angular-spring-data-rest/dist/angular-spring-data-rest.min.js',
  'node_modules/lf-ng-md-file-input/dist/lf-ng-md-file-input.js',
  'node_modules/lf-ng-md-file-input/dist/lf-ng-md-file-input.css',
  'node_modules/spin/dist/spin.min.js',
  'node_modules/angular-sanitize/angular-sanitize.min.js',
  'node_modules/showdown/dist/showdown.min.js',
  'node_modules/ng-showdown/dist/ng-showdown.min.js'
];

var excludeFiles = [
  'package.json',
  'readme.md',
  'gulpfile.js'
];

gulp.task('default', ['configLocal']);
gulp.task('build', ['clean', 'dist']);
gulp.task('dist', ['clean', 'copyBase', 'copyAssets', 'copyScripts', 'copyNodeModules']);

gulp.task('configLocal', function () {
  gulp.src('../config/clientConfig.json')
    .pipe(gulpNgConfig('fusewall.config', {
      environment: ['local', 'global']
    }))
    .pipe(rename('app.config.js'))
    .pipe(gulp.dest('./scripts/'));
});

gulp.task('configDev', function () {
  gulp.src('../config/clientConfig.json')
    .pipe(gulpNgConfig('fusewall.config', {
      environment: ['dev', 'global']
    }))
    .pipe(rename('app.config.js'))
    .pipe(gulp.dest('./scripts/'));
});

gulp.task('clean', function () {
  return gulp.src(buildPath, {read: false})
    .pipe(clean({force: true}));
});

gulp.task('copyBase', ['clean'], function () {
  return gulp.src('./*')
    .pipe(replace(cacheBustSearchString, cacheBustReplaceString))
    .pipe(filter(getExcludeFilter()))
    .pipe(gulp.dest(buildPath));
});

gulp.task('copyAssets', ['clean'], function () {
  return gulp.src('./assets/**/*')
    .pipe(replace(cacheBustSearchString, cacheBustReplaceString))
    .pipe(filter(getExcludeFilter()))
    .pipe(gulp.dest(buildPath + 'assets/'));
});

gulp.task('copyScripts', ['clean'], function () {
  return gulp.src('./scripts/**/*')
    .pipe(replace(cacheBustSearchString, cacheBustReplaceString))
    .pipe(filter(getExcludeFilter()))
    .pipe(gulp.dest(buildPath + 'scripts/'));
});

gulp.task('copyNodeModules', ['clean'], function () {
  _.each(includedExternalFiles, function (file) {
    gulp.src(file).pipe(gulp.dest(buildPath + file.substring(0, file.lastIndexOf("/"))));
  });
});

function getExcludeFilter() {
  var excludeFilter = _.map(excludeFiles, function (path) {
    return '!*' + path;
  });

  excludeFilter.unshift('**/*');
  return excludeFilter;
}
