FuseWall Web
====

This is the angular front end for the fuse wall. The documentation below describes how to update the end to end tests and other related topics.

HipTest - AUTOMATION
----

To get started with HipTest as the test reporter for the business unit and less technical members of the team use the following process:

 1. Install hip-test CLI:

        gem install hiptest-publisher

    For more reference information, use the following github page (https://github.com/hiptest/hiptest-publisher#hiptest-publisher). 

 1. Either generate a hiptest configuration file through this site (https://hiptest-publisher.scalingo.io/) or alternatively, you can use the CLI with all command line arguments to download the current project. The following shows an example of the CLI arguments for getting this information

        hiptest-publisher \
           --token=<secret_token_from_project> \
           --language=javascript \
           --framework=protractor \
           --test-run-id=<test_run_from_url>

        # The alternative version with configuration path:
        hiptest-publisher \
           --config-file=<config_file_path>

 1. Once you run the command you will get four files exported from your hiptest project. These are as follow:

    - *actionwords.js* - This contains definitions for action words. This is where all of the actual code editing is going to happen...but, preferrably as minimal as possible.
    - *actionwords_signature.yaml* - Signature for the methods and types that are used in the action words. This is used to generate the diff to determine which methods are new or have changed. The CLI uses this file or it can also be used manually.
    - *hiptest-publisher.conf* - Used to publish results of any runs back to hiptest
    - *project_test.js* - This is the actual runner and kicks off any scenarios. Should basically not need to be edited.

 1. Once the files describe above have been edited and the tests have been run it is preferable to use something like the jasmine JUnitXmlReporter in order to generate an output file that can be exported back to hip test. Depending on the automation around things the output file can be automatically uploaded via the following hiptest-publisher command:

        hiptest-publisher \
          --config-file=<hiptest-publisher.conf_file_path> \
          --push="<report_output_path>/*.xml"

That pretty much sums up the auomation for hiptest-publisher. If its not in here, you probably don't need to do it (KIDDING!).

HipTest - OTHER
----

The only other main thing to note about hip test is that all tests should rely on action words. Using this process ensures that the underlying automation can be re-used by the PSA, or business unit without requiring any code changes. Likewise, along the same lines it is important to separate action words that require automation from action words that just call other action words (which therefore doesn't require any automation). 

What else is out there?
----

So, of course protractor and hip test aren't the only end to end testing frameworks out there. Basically regardless of the framework the following are features to look for:

 - *Testing Level* - Is the automation using selenium or is it under the browser like Jasmine, JUnit, QUnit, etc
 - *Supported Frameworks* - What frameworks and languages does the tool support. If testing a front end jasmine is useful but when testing Java, obviously JUnit or something else makes more sense.
 - *Reporting* - What are the reporting options for results and how do those integrate with CI? This is one place where HipTest works well because it provides an easy reporting tool that is highly accessible for non-technical users. Along the same lines its worth considering whether or not the tool supports screen shots since these can be useful in diagnosing issues.
 - *Browser Support* - What browsers need to be tested? If a large range of browsers it may be worth considering sauce labs or browserstack.

So, with that in mind the following are a few other examples of options for testing stacks

 - *Unit testing*: Mocha, Jasmine, QUnit, etc
 - *Test Runners*: Testem, Karma
 - *End to End Testing* - Protractor, Nightwatch.js, cucumber, specflow, 
 - *Reporting/CI*: HipTest (via GEM), TestLodge (via REST API), possibly ci_reporter or other Jenkins plug ins

