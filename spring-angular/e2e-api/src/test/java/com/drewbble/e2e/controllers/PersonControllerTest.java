package com.drewbble.e2e.controllers;

import com.drewbble.e2e.dto.PersonRequest;
import com.drewbble.e2e.entities.Person;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.managers.PersonManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PersonControllerTest {
    @InjectMocks
    private PersonController subject;

    @Mock
    private PersonManager mockManager;

    @Mock
    private Person mockPerson;

    @Mock
    private HttpServletRequest mockRequest;

    private static final String bogusId = "bogusId";

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void canFindAllPersonsAsExpected() throws Exception {
        List<Person> fakeEverybodyList = new ArrayList<>();
        when(mockManager.findAll()).thenReturn(fakeEverybodyList);

        ResponseEntity<List<Person>> result = subject.getAll();

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(fakeEverybodyList, result.getBody());
    }

    @Test
    public void createsAPersonAsExpected() throws Exception {
        PersonRequest fakeRequest = new PersonRequest();
        when(mockManager.create(fakeRequest)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.createPerson(fakeRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void willDeleteAPersonAsExpected() throws Exception {
        ResponseEntity result = subject.delete(bogusId);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        verify(mockManager).delete(bogusId);
    }

    @Test
    public void getOneWorks_AdminNotSharedId() throws Exception {
        setIsAdminRole(true);
        when(mockManager.findOne(bogusId)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.get(bogusId, false, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void getOneWorks_AdminWithSharedId() throws Exception {
        setIsAdminRole(true);
        when(mockManager.findBySharedId(bogusId)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.get(bogusId, true, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void getOneWorks_ForceSharedInUUIDEditMode() throws Exception {
        setIsAdminRole(false);
        when(mockManager.findBySharedId(bogusId)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.get(bogusId, true, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void getSquadsWorks_AdminNotSharedId() throws Exception {
        setIsAdminRole(true);
        List<Squad> fakeSquadList = getFakeSquadList();
        when(mockManager.getSquads(bogusId)).thenReturn(fakeSquadList);

        ResponseEntity<List<Squad>> result = subject.getSquads(bogusId, false, mockRequest);

        assertOkResponseWithList(result, fakeSquadList);
    }

    @Test
    public void getSquadsWorks_AdminWithSharedId() throws Exception {
        setIsAdminRole(true);
        List<Squad> fakeSquadList = getFakeSquadList();
        when(mockManager.getSquadsBySharedId(bogusId)).thenReturn(fakeSquadList);

        ResponseEntity<List<Squad>> result = subject.getSquads(bogusId, true, mockRequest);

        assertOkResponseWithList(result, fakeSquadList);
    }

    @Test
    public void getSquadsWorks_ForceSharedInUUIDEditMode() throws Exception {
        setIsAdminRole(false);
        List<Squad> fakeSquadList = getFakeSquadList();
        when(mockManager.getSquadsBySharedId(bogusId)).thenReturn(fakeSquadList);

        ResponseEntity<List<Squad>> result = subject.getSquads(bogusId, false, mockRequest);

        assertOkResponseWithList(result, fakeSquadList);
    }

    @Test
    public void updatePersonWorks_AdminNotSharedId() throws Exception {
        setIsAdminRole(true);
        PersonRequest fakeRequest = new PersonRequest();
        when(mockManager.save(bogusId, fakeRequest)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.updatePerson(bogusId, fakeRequest, false, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void updatePersonWorks_AdminWithSharedId() throws Exception {
        setIsAdminRole(true);
        PersonRequest fakeRequest = new PersonRequest();
        when(mockManager.saveByShareableId(bogusId, fakeRequest)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.updatePerson(bogusId, fakeRequest, true, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void updatePersonWorks_ForceSharedInUUIDEditMode() throws Exception {
        setIsAdminRole(false);
        PersonRequest fakeRequest = new PersonRequest();
        when(mockManager.saveByShareableId(bogusId, fakeRequest)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.updatePerson(bogusId, fakeRequest, false, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void deleteAllSquadsWorks_AdminNotSharedId() throws Exception {
        setIsAdminRole(true);
        when(mockManager.removeAllSquads(bogusId)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.deleteAllSquads(bogusId, false, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void deleteAllSquadsWorks_AdminWithSharedId() throws Exception {
        setIsAdminRole(true);
        when(mockManager.removeAllSquadsByShareableId(bogusId)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.deleteAllSquads(bogusId, true, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void deleteAllSquadsWorks_ForceSharedInUUIDEditMode() throws Exception {
        setIsAdminRole(false);
        when(mockManager.removeAllSquadsByShareableId(bogusId)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.deleteAllSquads(bogusId, false, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void addSquadWorks_AdminNotSharedId() throws Exception {
        setIsAdminRole(true);
        String bogusSquadId = "bogusSquadId";
        when(mockManager.addSquad(bogusId, bogusSquadId)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.addSquad(bogusId, bogusSquadId, false, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void addSquadWorks_AdminWithSharedId() throws Exception {
        setIsAdminRole(true);
        String bogusSquadId = "bogusSquadId";
        when(mockManager.addSquadByShareableId(bogusId, bogusSquadId)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.addSquad(bogusId, bogusSquadId, true, mockRequest);

        assertOkResponseWithPerson(result);
    }

    @Test
    public void addSquadWorks_ForceSharedInUUIDEditMode() throws Exception {
        setIsAdminRole(false);
        String bogusSquadId = "bogusSquadId";
        when(mockManager.addSquadByShareableId(bogusId, bogusSquadId)).thenReturn(mockPerson);

        ResponseEntity<Person> result = subject.addSquad(bogusId, bogusSquadId, false, mockRequest);

        assertOkResponseWithPerson(result);
    }

    private void assertOkResponseWithPerson(ResponseEntity<Person> result) {
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(mockPerson, result.getBody());
    }

    private <T> void assertOkResponseWithList(ResponseEntity<List<T>> result, List<T> expected) {
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expected, result.getBody());
    }

    private void setIsAdminRole(boolean isAdmin) {
        when(mockRequest.isUserInRole("uuidEdit")).thenReturn(!isAdmin);
    }

    private List<Squad> getFakeSquadList() {
        return Collections.singletonList(new Squad());
    }
}