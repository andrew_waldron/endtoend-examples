package com.drewbble.e2e.controllers;


import com.drewbble.e2e.dto.ProductRequest;
import com.drewbble.e2e.entities.Product;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.managers.ProductManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProductControllerTest {
    @Mock
    private ProductManager mockManager;

    @Mock
    private Product mockProduct;

    @InjectMocks
    private ProductController subject;

    private static final String bogusId = "bogusId";

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCallsAppropriateManagerMethodAndReturnsTheResult() throws Exception {
        when(mockManager.findOne(bogusId)).thenReturn(mockProduct);

        ResponseEntity<Product> result = subject.get(bogusId);

        assertOkResponseWithProduct(result);
    }

    @Test
    public void canCreateAProduct() throws Exception {
        ProductRequest fakeRequest = new ProductRequest();
        when(mockManager.create(fakeRequest)).thenReturn(mockProduct);

        ResponseEntity<Product> result = subject.createProduct(fakeRequest);

        assertOkResponseWithProduct(result);
    }

    @Test
    public void canUpdateAProduct() throws Exception {
        ProductRequest fakeRequest = new ProductRequest();
        when(mockManager.save(bogusId, fakeRequest)).thenReturn(mockProduct);

        ResponseEntity<Product> result = subject.updateProduct(bogusId, fakeRequest);

        assertOkResponseWithProduct(result);
    }

    @Test
    public void willDeleteAProductAsExpected() throws Exception {
        ResponseEntity result = subject.delete(bogusId);

        verify(mockManager).delete(bogusId);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void willFindAllProducts() throws Exception {
        List<Product> allProducts = new ArrayList<>();
        when(mockManager.findAll()).thenReturn(allProducts);

        ResponseEntity<List<Product>> result = subject.getAll();

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(allProducts, result.getBody());
    }

    @Test
    public void willGetAListOfSquadsForAParticularProduct() throws Exception {
        List<Squad> squadList = new ArrayList<>();
        when(mockManager.getSquads(bogusId)).thenReturn(squadList);

        ResponseEntity<List<Squad>> result = subject.getSquads(bogusId);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(squadList, result.getBody());
    }

    @Test
    public void willDeleteAllSquadsForAProduct() throws Exception {
        when(mockManager.removeAllSquads(bogusId)).thenReturn(mockProduct);

        ResponseEntity<Product> result = subject.deleteAllSquads(bogusId);

        assertOkResponseWithProduct(result);
    }

    @Test
    public void addsASquadToTheProductAsExpected() throws Exception {
        String bogusSquadId = "someBogusSquadId";
        when(mockManager.addSquad(bogusId, bogusSquadId)).thenReturn(mockProduct);

        ResponseEntity<Product> result = subject.addSquad(bogusId, bogusSquadId);

        assertOkResponseWithProduct(result);
    }

    private void assertOkResponseWithProduct(ResponseEntity<Product> result) {
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(mockProduct, result.getBody());
    }
}