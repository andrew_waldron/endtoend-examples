package com.drewbble.e2e.managers;

import com.drewbble.e2e.dto.GraphNodeDTO;
import com.drewbble.e2e.dto.GraphNodeLinkDTO;
import com.drewbble.e2e.dto.PersonRequest;
import com.drewbble.e2e.entities.Person;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.repositories.PersonRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class PersonManagerTest {
    @InjectMocks
    private PersonManager subject;

    @Mock
    private PersonRepository mockPersonRepository;

    @Mock
    private SquadManager mockSquadManager;

    private String bogusId;
    private Person sharablePerson;
    private Person normalPerson;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        bogusId = "barf";
        sharablePerson = new Person();
        normalPerson = new Person();

        when(mockPersonRepository.findOneByShareableId(bogusId)).thenReturn(sharablePerson);
        when(mockPersonRepository.findOne(bogusId)).thenReturn(normalPerson);
    }

    @Test
    public void itFindsUsingTheRepository() throws Exception {
        Person result = subject.findOne(bogusId);

        assertEquals(normalPerson, result);
    }

    @Test
    public void itFindsBySharedIdAsExpected() throws Exception {
        Person result = subject.findBySharedId(bogusId);

        assertEquals(sharablePerson, result);
    }

    @Test
    public void itCanFindAllAsExpected() throws Exception {
        List<Person> allThePeople = new ArrayList<>();
        when(mockPersonRepository.findAll()).thenReturn(allThePeople);

        List<Person> result = subject.findAll();

        assertEquals(allThePeople, result);
    }

    @Test
    public void itCanGetSquadsByASharedId() throws Exception {
        List<Squad> expected = new ArrayList<>();
        sharablePerson.setSquads(expected);

        List<Squad> result = subject.getSquadsBySharedId(bogusId);

        assertEquals(expected, result);
    }

    @Test
    public void itReturnsNullForTheSquadsListWhenThePersonDoesntExist() throws Exception {
        assertNull(subject.getSquadsBySharedId("invalidId"));
    }

    @Test
    public void itCanGetSquadsNormally() throws Exception {
        List<Squad> expected = new ArrayList<>();
        normalPerson.setSquads(expected);

        List<Squad> result = subject.getSquads(bogusId);

        assertEquals(expected, result);
    }

    @Test
    public void itReturnsNullForTheSquadsListWhenTheNormalPersonDoesntExist() throws Exception {
        assertNull(subject.getSquads("invalidId"));
    }

    @Test
    public void itWillSavePersonUpdatesGivenADTO() throws Exception {
        when(mockPersonRepository.save(any(Person.class))).thenReturn(normalPerson);
        PersonRequest request = getStandardRequest();

        Person result = subject.save(bogusId, request);

        assertEquals(request.getTitle(), result.getTitle());
        assertEquals(request.getDescription(), result.getDescription());
        assertEquals(request.getImageUrl(), result.getImageUrl());
    }

    @Test
    public void ifNewSaveHasNoImageKeepsExistingImage() throws Exception {
        normalPerson.setImageUrl("originalImage");
        when(mockPersonRepository.save(any(Person.class))).thenReturn(normalPerson);

        Person result = subject.save(bogusId, new PersonRequest());

        assertEquals("originalImage", result.getImageUrl());
    }

    @Test
    public void whenPassedAnInvalidIdDoesNotSave() throws Exception {
        Person result = subject.save("invalidId", new PersonRequest());

        assertNull(result);
        verify(mockPersonRepository, never()).save(any(Person.class));
    }

    @Test
    public void whenPassedNoPersonIdDoesNotRequestFind() throws Exception {
        subject.save(null, new PersonRequest());

        verify(mockPersonRepository, never()).findOne(anyString());
    }

    @Test
    public void whenCreateIsCalledItCreatesANewPerson() throws Exception {
        Person saveResult = new Person();
        PersonRequest request = getStandardRequest();
        when(mockPersonRepository.save(any(Person.class))).thenReturn(saveResult);

        Person saved = subject.create(request);

        assertEquals(saveResult, saved);
        ArgumentCaptor<Person> capture = ArgumentCaptor.forClass(Person.class);
        verify(mockPersonRepository).save(capture.capture());
        assertPersonMatchesRequest(request, capture.getValue());
        assertNull(capture.getValue().getPersonId());
    }

    @Test
    public void whenSavingBySharableIdItUpdatesTheBasicProperties() throws Exception {
        when(mockPersonRepository.save(any(Person.class))).thenReturn(sharablePerson);

        PersonRequest request = getStandardRequest();
        Person result = subject.saveByShareableId(bogusId, request);

        assertPersonMatchesRequest(request, result);
    }

    @Test
    public void whenSavingByAnInvalidSharableIdItDoesNothing() throws Exception {
        Person result = subject.saveByShareableId("invalidId", new PersonRequest());

        assertNull(result);
        verify(mockPersonRepository, never()).save(any(Person.class));
    }

    @Test
    public void whenSavingByAnNullSharableIdItDoesntTryToLookThePersonUp() throws Exception {
        subject.saveByShareableId(null, new PersonRequest());

        verify(mockPersonRepository, never()).findOne(anyString());
    }

    @Test
    public void itGetsAPersonDTOCorrectly() throws Exception {
        normalPerson.setPersonId("something");
        normalPerson.setTitle("someTitle");
        normalPerson.setDescription("someDescr");
        normalPerson.setImageUrl("someImage");

        GraphNodeDTO node = subject.getPersonDTO(bogusId);

        assertEquals(normalPerson.getPersonId(), node.getId());
        assertEquals(normalPerson.getTitle(), node.getTitle());
        assertEquals(normalPerson.getDescription(), node.getDescription());
        assertEquals(normalPerson.getImageUrl(), node.getImageUrl());
        assertEquals("people", node.getNodeType());
    }

    @Test
    public void whenGettingAllPeopleItConvertsTheResultsOfFindAllToPeopleDTOsWithoutImages() throws Exception {
        List<Person> allThePeople = Arrays.asList(getSamplePerson(1), getSamplePerson(2), getSamplePerson(3));
        when(mockPersonRepository.findAll()).thenReturn(allThePeople);

        List<GraphNodeDTO> graphDTO = subject.getAllPeopleDTO();

        IntStream.range(0, graphDTO.size())
                .forEach(index -> assertNodeIsCorrect(index + 1, graphDTO.get(index)));
    }

    @Test
    public void runsDeleteAsExpected() throws Exception {
        subject.delete(bogusId);

        verify(mockPersonRepository).delete(bogusId);
    }

    @Test
    public void canAddASquad() throws Exception {
        String bogusSquadId = "someSquadId";

        Person result = subject.addSquad(bogusId, bogusSquadId);

        verify(mockSquadManager).addPerson(bogusSquadId, bogusId);
        assertEquals(normalPerson, result);
    }

    @Test
    public void canAddASquadBySharableId() throws Exception {
        String bogusSquadId = "someSquadId";
        sharablePerson.setPersonId(bogusId);

        Person result = subject.addSquadByShareableId(bogusId, bogusSquadId);

        verify(mockSquadManager).addPerson(bogusSquadId, bogusId);
        assertEquals(normalPerson, result);
    }

    @Test
    public void justReturnsANullAndDoesNothingWhenInvalidShareIdIsPassedIn() throws Exception {
        String bogusSquadId = "someSquadId";

        Person result = subject.addSquadByShareableId("invalidId", bogusSquadId);

        verify(mockSquadManager, never()).addPerson(anyString(), anyString());
        assertNull(result);
    }

    @Test
    public void canRemoveAllSquads() throws Exception {
        setupForSquadRemoval();

        Person result = subject.removeAllSquads(bogusId);

        assertEquals(normalPerson, result);
        ArgumentCaptor<Person> captor = ArgumentCaptor.forClass(Person.class);
        verify(mockPersonRepository).save(captor.capture());
        assertEquals(0, captor.getValue().getSquads().size());
    }

    @Test
    public void doesNotDoAnythingWhenPassedAnInvalidPersonIdToClearSquadListFor() throws Exception {
        Person result = subject.removeAllSquads("invalidId");

        assertNull(result);
        verify(mockPersonRepository, never()).save(any(Person.class));
    }

    @Test
    public void canRemoveAllSquadsByShareableId() throws Exception {
        setupForSquadRemoval();
        sharablePerson.setPersonId(bogusId);

        Person result = subject.removeAllSquadsByShareableId(bogusId);

        assertEquals(normalPerson, result);
        ArgumentCaptor<Person> captor = ArgumentCaptor.forClass(Person.class);
        verify(mockPersonRepository).save(captor.capture());
        assertEquals(sharablePerson, captor.getValue());
        assertEquals(0, captor.getValue().getSquads().size());
    }

    @Test
    public void getsAllPeopleLinksWhenRequested() throws Exception {
        List<Person> allThePeople = Arrays.asList(getSamplePersonWithBogusLinks(1), getSamplePersonWithBogusLinks(2));
        when(mockPersonRepository.findAll()).thenReturn(allThePeople);

        List<GraphNodeLinkDTO> graphNodes = subject.getAllPeopleLinks();

        assertEquals(4, graphNodes.size());
        assertPersonToTeamLinkExists(graphNodes, 1, 1);
        assertPersonToTeamLinkExists(graphNodes, 1, 2);
        assertPersonToTeamLinkExists(graphNodes, 2, 1);
        assertPersonToTeamLinkExists(graphNodes, 2, 2);
    }

    private void assertPersonToTeamLinkExists(List<GraphNodeLinkDTO> graphNodes, Integer personId, Integer teamId) {
        assertTrue(graphNodes.stream().anyMatch(node -> Objects.equals(node.getSource(), teamId.toString()) &&
                Objects.equals(node.getTarget(), personId.toString()) &&
                Objects.equals(node.getSourceType(), "team")));
    }

    private Person getSamplePersonWithBogusLinks(int id) {
        Person sample = getSamplePerson(id);
        sample.setSquads(Arrays.asList(createFakeSquad(1), createFakeSquad(2)));
        return sample;
    }

    private Squad createFakeSquad(Integer squadId) {
        Squad fakeSquad = new Squad();
        fakeSquad.setSquadId(squadId.toString());
        return fakeSquad;
    }

    private void setupForSquadRemoval() {
        when(mockPersonRepository.save(any(Person.class))).thenReturn(normalPerson);
        List<Squad> mutableList = new ArrayList<>();
        mutableList.add(new Squad());
        normalPerson.setSquads(mutableList);
        sharablePerson.setSquads(mutableList);
    }

    private void assertNodeIsCorrect(Integer index, GraphNodeDTO graphNodeDTO) {
        String uniqueId = index.toString();
        assertEquals(uniqueId, graphNodeDTO.getId());
        assertEquals("title" + uniqueId, graphNodeDTO.getTitle());
        assertEquals("description" + uniqueId, graphNodeDTO.getDescription());
        assertEquals(null, graphNodeDTO.getImageUrl());
        assertEquals("people", graphNodeDTO.getNodeType());
    }

    private Person getSamplePerson(Integer personId) {
        Person person = new Person();

        String uniqueId = personId.toString();
        person.setPersonId(uniqueId);
        person.setTitle("title" + uniqueId);
        person.setDescription("description" + uniqueId);
        person.setImageUrl("image" + uniqueId);

        return person;
    }

    private void assertPersonMatchesRequest(PersonRequest request, Person result) {
        assertEquals(request.getTitle(), result.getTitle());
        assertEquals(request.getDescription(), result.getDescription());
        assertEquals(request.getImageUrl(), result.getImageUrl());
    }

    private PersonRequest getStandardRequest() {
        PersonRequest request = new PersonRequest();
        request.setTitle("title");
        request.setDescription("description");
        request.setImageUrl("imageUrl");
        return request;
    }
}