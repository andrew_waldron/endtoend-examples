package com.drewbble.e2e.entities;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProductTest {
    private Product subject;

    @Before
    public void setup() throws Exception {
        subject = new Product();
    }

    @Test
    public void getsAndSetsAllPropertiesAsExpected() throws Exception {
        String someString = "goodStuff";
        Date someDate = new Date();
        List<Squad> someList = new ArrayList<>();

        subject.setProductId(someString);
        subject.setCreatedBy(someString);
        subject.setDescription(someString);
        subject.setImageUrl(someString);
        subject.setTitle(someString);
        subject.setLastModifiedBy(someString);
        subject.setCreatedDate(someDate);
        subject.setLastModifiedDate(someDate);
        subject.setSquads(someList);

        assertEquals(someString, subject.getProductId());
        assertEquals(someString, subject.getCreatedBy());
        assertEquals(someString, subject.getDescription());
        assertEquals(someString, subject.getImageUrl());
        assertEquals(someString, subject.getTitle());
        assertEquals(someString, subject.getLastModifiedBy());
        assertEquals(someDate, subject.getCreatedDate());
        assertEquals(someDate, subject.getLastModifiedDate());
        assertEquals(someList, subject.getSquads());
    }
}