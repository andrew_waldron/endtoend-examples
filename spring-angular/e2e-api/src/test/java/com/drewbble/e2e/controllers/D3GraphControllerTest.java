package com.drewbble.e2e.controllers;

import com.drewbble.e2e.dto.GraphNodeDTO;
import com.drewbble.e2e.dto.GraphNodeLinkDTO;
import com.drewbble.e2e.dto.Organization;
import com.drewbble.e2e.managers.PersonManager;
import com.drewbble.e2e.managers.ProductManager;
import com.drewbble.e2e.managers.SquadManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class D3GraphControllerTest {
    @InjectMocks
    private D3GraphController subject;

    @Mock
    private ProductManager mockProductManager;

    @Mock
    private PersonManager mockPersonManager;

    @Mock
    private SquadManager mockSquadManager;

    private List<GraphNodeDTO> productDTO;
    private List<GraphNodeDTO> personDTO;
    private List<GraphNodeDTO> squadDTO;
    private List<GraphNodeLinkDTO> personLinks;
    private List<GraphNodeLinkDTO> productLinks;
    private List<GraphNodeLinkDTO> squadLinks;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        setupFakeListsAndConnectToGraphCalls();
    }

    @Test
    public void itGetsTheEntireGraphAsExpected() throws Exception {
        Organization result = subject.getEntireGraph();

        validateAllLinksContained(result, personLinks);
        validateAllLinksContained(result, productLinks);
        validateAllLinksContained(result, squadLinks);
        assertEquals(productLinks.size() + personLinks.size() + squadLinks.size(), result.getLinks().size());

        validateAllNodesContained(result, productDTO);
        validateAllNodesContained(result, personDTO);
        validateAllNodesContained(result, squadDTO);
        assertEquals(productDTO.size() + personDTO.size() + squadDTO.size() + 1, result.getNodes().size());
    }

    @Test
    public void itPlacesAHardCodedOrganizationInTheGraph() throws Exception {
        subject.setFuseImage("somePretendImage");

        Organization result = subject.getEntireGraph();

        GraphNodeDTO hardCodedCenter = result.getNodes().get(0);
        assertEquals("1", hardCodedCenter.getId());
        assertEquals("Fuse", hardCodedCenter.getTitle());
        assertEquals("Fuse by Cardinal Health", hardCodedCenter.getDescription());
        assertEquals(subject.getFuseImage(), hardCodedCenter.getImageUrl());
        assertEquals("org", hardCodedCenter.getNodeType());
    }

    @Test
    public void itGetsPersonNodeDetailsWhenAvailable() throws Exception {
        String bogusPersonId = "bogusPersonId";
        GraphNodeDTO fakeNode = new GraphNodeDTO("1", "2", "3", "4", "5");
        when(mockPersonManager.getPersonDTO(bogusPersonId)).thenReturn(fakeNode);

        GraphNodeDTO result = subject.getNodeDetails(bogusPersonId);

        assertEquals(fakeNode, result);
    }

    @Test
    public void whenPassedAProductIdItGetsThoseDetails() throws Exception {
        String bogusProductId = "bogusProductId";
        GraphNodeDTO fakeNode = new GraphNodeDTO("1", "2", "3", "4", "5");
        when(mockProductManager.getProductDTO(bogusProductId)).thenReturn(fakeNode);

        GraphNodeDTO result = subject.getNodeDetails(bogusProductId);

        assertEquals(fakeNode, result);
    }

    @Test
    public void whenPassedASquadIdItGetsThoseDetails() throws Exception {
        String bogusSquadId = "bogusSquadId";
        GraphNodeDTO fakeNode = new GraphNodeDTO("1", "2", "3", "4", "5");
        when(mockSquadManager.getSquadDTO(bogusSquadId)).thenReturn(fakeNode);

        GraphNodeDTO result = subject.getNodeDetails(bogusSquadId);

        assertEquals(fakeNode, result);
    }

    @Test
    public void whenPassedACompletelyInvalidIdItReturnsNull() throws Exception {
        assertNull(subject.getNodeDetails("terribleId"));
    }

    private void setupFakeListsAndConnectToGraphCalls() {
        productDTO = createFakeDTOList("product");
        personDTO = createFakeDTOList("person");
        squadDTO = createFakeDTOList("squad");
        personLinks = createFakeLinksList("person");
        productLinks = createFakeLinksList("product");
        squadLinks = createFakeLinksList("squad");

        setupGraphCalls(productDTO, personDTO, squadDTO, personLinks, productLinks, squadLinks);
    }

    private void setupGraphCalls(List<GraphNodeDTO> productDTO, List<GraphNodeDTO> personDTO, List<GraphNodeDTO> squadDTO, List<GraphNodeLinkDTO> personLinks, List<GraphNodeLinkDTO> productLinks, List<GraphNodeLinkDTO> squadLinks) {
        when(mockProductManager.getAllProductsDTO()).thenReturn(productDTO);
        when(mockPersonManager.getAllPeopleDTO()).thenReturn(personDTO);
        when(mockSquadManager.getAllSquadsDTO()).thenReturn(squadDTO);
        when(mockProductManager.getAllProductsLinks()).thenReturn(personLinks);
        when(mockPersonManager.getAllPeopleLinks()).thenReturn(productLinks);
        when(mockSquadManager.getAllSquadsLinks()).thenReturn(squadLinks);
    }

    private void validateAllNodesContained(Organization result, List<GraphNodeDTO> nodes) {
        nodes.forEach(node -> {
            assertTrue(result.getNodes().stream().anyMatch(resultNode -> resultNode.equals(node)));
        });
    }

    private void validateAllLinksContained(Organization result, List<GraphNodeLinkDTO> links) {
        links.forEach(link -> {
            assertTrue(result.getLinks().stream().anyMatch(resultLink -> resultLink.equals(link)));
        });
    }

    private List<GraphNodeDTO> createFakeDTOList(String listType) {
        return Arrays.asList(getFakeNode(listType, 0), getFakeNode(listType, 1));
    }

    private GraphNodeDTO getFakeNode(String listType, Integer uniqueness) {
        return new GraphNodeDTO(uniqueness.toString(),
                "someTitle" + uniqueness,
                "someDescription" + uniqueness,
                "someImageUrl" + uniqueness,
                listType);
    }

    private List<GraphNodeLinkDTO> createFakeLinksList(String listType) {
        return Arrays.asList(getFakeLink(listType, 0), getFakeLink(listType, 1));
    }

    private GraphNodeLinkDTO getFakeLink(String listType, Integer uniqueness) {
        return new GraphNodeLinkDTO(uniqueness.toString(), "target" + uniqueness, listType);
    }

}