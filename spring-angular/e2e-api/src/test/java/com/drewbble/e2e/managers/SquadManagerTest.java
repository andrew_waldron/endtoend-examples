package com.drewbble.e2e.managers;

import com.drewbble.e2e.dto.GraphNodeDTO;
import com.drewbble.e2e.dto.GraphNodeLinkDTO;
import com.drewbble.e2e.dto.SquadRequest;
import com.drewbble.e2e.entities.Person;
import com.drewbble.e2e.entities.Product;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.repositories.SquadRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class SquadManagerTest {
    @InjectMocks
    private SquadManager subject;

    @Mock
    private PersonManager mockPersonManager;

    @Mock
    private ProductManager mockProductManager;

    @Mock
    private SquadRepository mockSquadRepository;

    private Squad fakeSquad;
    private static final String bogusId = "bogusForSure";
    private static final String bogusProductId = "bogusProductId";
    private static final String bogusPersonId = "bogusPersonId";

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        fakeSquad = new Squad();
        when(mockSquadRepository.findOne(bogusId)).thenReturn(fakeSquad);
        when(mockSquadRepository.save(any(Squad.class))).thenReturn(fakeSquad);
    }

    @Test
    public void findAllFindsAll() throws Exception {
        List<Squad> randomList = Collections.singletonList(new Squad());
        when(mockSquadRepository.findAll()).thenReturn(randomList);

        List<Squad> result = subject.findAll();

        assertEquals(randomList, result);
    }

    @Test
    public void findOneUsesFindOne() throws Exception {
        Squad result = subject.findOne(bogusId);

        assertEquals(fakeSquad, result);
    }

    @Test
    public void whenSavingItUpdatesTheProperties() throws Exception {
        SquadRequest fakeRequest = getFakeRequest();

        Squad returnedSquad = subject.save(bogusId, fakeRequest);

        assertEquals(returnedSquad, fakeSquad);
        ArgumentCaptor<Squad> captor = ArgumentCaptor.forClass(Squad.class);
        verify(mockSquadRepository).save(captor.capture());
        assertSquadsEqual(fakeRequest, captor.getValue());
    }

    @Test
    public void itLeavesTheSquadImageAloneIfNoNewImageIsProvided() throws Exception {
        SquadRequest fakeRequest = getFakeRequest();
        fakeRequest.setImageUrl("");
        fakeSquad.setImageUrl("someImageUrl");

        subject.save(bogusId, fakeRequest);

        ArgumentCaptor<Squad> captor = ArgumentCaptor.forClass(Squad.class);
        verify(mockSquadRepository).save(captor.capture());
        assertEquals("someImageUrl", captor.getValue().getImageUrl());
    }

    @Test
    public void whenNoSquadIsFoundSaveDoesNothing() throws Exception {
        Squad saved = subject.save("invalidId", new SquadRequest());

        assertNull(saved);
        verify(mockSquadRepository, never()).save(any(Squad.class));
    }

    @Test
    public void whenSquadIdIsNullDoesntEvenTryToFindTheSquad() throws Exception {
        subject.save(null, new SquadRequest());

        verify(mockSquadRepository, never()).findOne(anyString());
    }

    @Test
    public void whenCreateIsCalledItCallsSavedWithAnAppropriatelyMappedSquad() throws Exception {
        SquadRequest fakeRequest = getFakeRequest();

        Squad result = subject.create(fakeRequest);

        assertEquals(fakeSquad, result);
        ArgumentCaptor<Squad> captor = ArgumentCaptor.forClass(Squad.class);
        verify(mockSquadRepository).save(captor.capture());
        assertSquadsEqual(fakeRequest, captor.getValue());
    }

    @Test
    public void deletesASquadAsExpected() throws Exception {
        subject.delete(bogusId);

        verify(mockSquadRepository).delete(bogusId);
    }

    @Test
    public void addsAProductAsExpected() throws Exception {
        Product fakeProduct = new Product();
        when(mockProductManager.findOne(bogusProductId)).thenReturn(fakeProduct);
        fakeSquad.setProducts(new ArrayList<>());

        Squad result = subject.addProduct(bogusId, bogusProductId);

        assertEquals(fakeSquad, result);
        verify(mockSquadRepository).save(fakeSquad);
        assertEquals(fakeProduct, fakeSquad.getProducts().get(0));
    }

    @Test
    public void whenProductIsAlreadyAddedDoesNothing() throws Exception {
        Product fakeProduct = new Product();
        fakeProduct.setProductId(bogusProductId);
        when(mockProductManager.findOne(bogusProductId)).thenReturn(fakeProduct);
        fakeSquad.setProducts(new ArrayList<>(Collections.singletonList(fakeProduct)));

        Squad result = subject.addProduct(bogusId, bogusProductId);

        assertEquals(fakeSquad, result);
        assertEquals(1, result.getProducts().size());
    }

    @Test
    public void whenNoProductIsFoundDoesNothing() throws Exception {
        fakeSquad.setProducts(new ArrayList<>());

        Squad result = subject.addProduct(bogusId, bogusProductId);

        assertEquals(fakeSquad, result);
        assertTrue(result.getProducts().isEmpty());
        verify(mockSquadRepository, never()).save(any(Squad.class));
    }

    @Test
    public void whenABogusSquadIdIsPassedItAlsoDoesNothing() throws Exception {
        assertNull(subject.addProduct("invalidId", bogusProductId));
    }

    @Test
    public void addsAPersonAsExpected() throws Exception {
        Person fakePerson = new Person();
        when(mockPersonManager.findOne(bogusPersonId)).thenReturn(fakePerson);
        fakeSquad.setSquadMembers(new ArrayList<>());

        Squad result = subject.addPerson(bogusId, bogusPersonId);

        assertEquals(fakeSquad, result);
        verify(mockSquadRepository).save(fakeSquad);
        assertEquals(fakePerson, fakeSquad.getSquadMembers().get(0));
    }

    @Test
    public void whenPersonIsAlreadyAddedDoesNothing() throws Exception {
        Person fakePerson = new Person();
        fakePerson.setPersonId(bogusPersonId);
        when(mockPersonManager.findOne(bogusPersonId)).thenReturn(fakePerson);
        fakeSquad.setSquadMembers(new ArrayList<>(Collections.singletonList(fakePerson)));

        Squad result = subject.addPerson(bogusId, bogusPersonId);

        assertEquals(fakeSquad, result);
        assertEquals(1, result.getSquadMembers().size());
    }

    @Test
    public void whenNoPersonIsFoundDoesNothing() throws Exception {
        fakeSquad.setSquadMembers(new ArrayList<>());

        Squad result = subject.addPerson(bogusId, bogusPersonId);

        assertEquals(fakeSquad, result);
        assertTrue(result.getSquadMembers().isEmpty());
        verify(mockSquadRepository, never()).save(any(Squad.class));
    }

    @Test
    public void whenABogusSquadIdIsPassedAddingAPersonAlsoDoesNothing() throws Exception {
        assertNull(subject.addPerson("invalidId", bogusPersonId));
    }

    @Test
    public void canGetASingleSquadDTOObjectCorrectly() throws Exception {
        fakeSquad.setTitle("title");
        fakeSquad.setImageUrl("imageUrl");
        fakeSquad.setDescription("description");
        fakeSquad.setSquadId("squadId");

        GraphNodeDTO result = subject.getSquadDTO(bogusId);

        assertSquadMatchesDTO(fakeSquad, result);
    }

    @Test
    public void whenGettingASingleSquadDTOReturnsNullWhenNothingIsFound() throws Exception {
        assertNull(subject.getSquadDTO("invalidId"));
    }

    @Test
    public void getsAllSquadsAsDTOObjectsCorrectly() throws Exception {
        List<Squad> fakeSquadList = Arrays.asList(getFakeSquad(1), getFakeSquad(2), getFakeSquad(3));
        when(mockSquadRepository.findAll()).thenReturn(fakeSquadList);

        List<GraphNodeDTO> result = subject.getAllSquadsDTO();

        assertEquals(fakeSquadList.size(), result.size());
        IntStream.range(0, result.size())
                .forEach(index -> assertSquadMatchesDTO(fakeSquadList.get(index), result.get(index)));
    }

    @Test
    public void canCorrectlyGetAListOfAllSquadLinksWhichPointToTheCurrentOrg() throws Exception {
        List<Squad> fakeSquadList = Arrays.asList(getFakeSquad(1), getFakeSquad(2), getFakeSquad(3));
        when(mockSquadRepository.findAll()).thenReturn(fakeSquadList);

        List<GraphNodeLinkDTO> result = subject.getAllSquadsLinks();

        assertEquals(fakeSquadList.size(), result.size());
        IntStream.range(0, result.size())
                .forEach(index -> assertSquadIsLinkedToCenter(fakeSquadList.get(index), result.get(index)));
    }

    private void assertSquadIsLinkedToCenter(Squad squad, GraphNodeLinkDTO link) {
        assertEquals("1", link.getSource());
        assertEquals("org", link.getSourceType());
        assertEquals(squad.getSquadId(), link.getTarget());
    }

    private Squad getFakeSquad(int uniquenessNumber) {
        Squad fake = new Squad();
        fake.setTitle("title" + uniquenessNumber);
        fake.setImageUrl("imageUrl" + uniquenessNumber);
        fake.setDescription("description" + uniquenessNumber);
        fake.setSquadId("squadId" + uniquenessNumber);
        return fake;
    }

    private void assertSquadMatchesDTO(Squad squad, GraphNodeDTO dto) {
        assertEquals(squad.getTitle(), dto.getTitle());
        assertEquals(squad.getImageUrl(), dto.getImageUrl());
        assertEquals(squad.getDescription(), dto.getDescription());
        assertEquals(squad.getSquadId(), dto.getId());
        assertEquals("team", dto.getNodeType());
    }

    private void assertSquadsEqual(SquadRequest expected, Squad actual) {
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getTitle(), actual.getTitle());
        assertEquals(expected.getImageUrl(), actual.getImageUrl());
    }

    private SquadRequest getFakeRequest() {
        SquadRequest fake = new SquadRequest();
        fake.setDescription("someDescription");
        fake.setImageUrl("someImage");
        fake.setTitle("someTitle");
        return fake;
    }
}