package com.drewbble.e2e.entities;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PersonTest {
    private Person subject;

    @Before
    public void setup() throws Exception {
        subject = new Person();
    }

    @Test
    public void getsAndSetsAllPropertiesAsExpected() throws Exception {
        String someString = "goodStuff";
        Date someDate = new Date();
        List<Squad> someList = new ArrayList<>();

        subject.setCreatedBy(someString);
        subject.setDescription(someString);
        subject.setImageUrl(someString);
        subject.setTitle(someString);
        subject.setLastModifiedBy(someString);
        subject.setCreatedDate(someDate);
        subject.setLastModifiedDate(someDate);
        subject.setPersonId(someString);
        subject.setSquads(someList);
        subject.setShareableId(someString);

        assertEquals(someString, subject.getCreatedBy());
        assertEquals(someString, subject.getDescription());
        assertEquals(someString, subject.getImageUrl());
        assertEquals(someString, subject.getTitle());
        assertEquals(someString, subject.getLastModifiedBy());
        assertEquals(someString, subject.getPersonId());
        assertEquals(someString, subject.getShareableId());
        assertEquals(someDate, subject.getCreatedDate());
        assertEquals(someDate, subject.getLastModifiedDate());
        assertEquals(someList, subject.getSquads());
    }
}
