package com.drewbble.e2e.entities;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SquadTest {
    private Squad subject;

    @Before
    public void setup() throws Exception {
        subject = new Squad();
    }

    @Test
    public void getsAndSetsAllPropertiesAsExpected() throws Exception {
        String someString = "goodStuff";
        Date someDate = new Date();
        List<Product> someListOfProducts = new ArrayList<>();
        List<Person> someListOfPeople = new ArrayList<>();

        subject.setSquadId(someString);
        subject.setCreatedBy(someString);
        subject.setDescription(someString);
        subject.setImageUrl(someString);
        subject.setTitle(someString);
        subject.setLastModifiedBy(someString);
        subject.setCreatedDate(someDate);
        subject.setLastModifiedDate(someDate);
        subject.setProducts(someListOfProducts);
        subject.setSquadMembers(someListOfPeople);

        assertEquals(someString, subject.getSquadId());
        assertEquals(someString, subject.getCreatedBy());
        assertEquals(someString, subject.getDescription());
        assertEquals(someString, subject.getImageUrl());
        assertEquals(someString, subject.getTitle());
        assertEquals(someString, subject.getLastModifiedBy());
        assertEquals(someDate, subject.getCreatedDate());
        assertEquals(someDate, subject.getLastModifiedDate());
        assertEquals(someListOfProducts, subject.getProducts());
        assertEquals(someListOfPeople, subject.getSquadMembers());
    }
}