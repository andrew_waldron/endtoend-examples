package com.drewbble.e2e.controllers;

import com.drewbble.e2e.dto.SquadRequest;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.managers.SquadManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SquadControllerTest {
    @Mock
    private SquadManager mockManager;

    @InjectMocks
    private SquadController subject;

    private static final String bogusId = "bogusSquadId";

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findsAllAsExpected() throws Exception {
        List<Squad> expected = Collections.singletonList(new Squad());
        when(mockManager.findAll()).thenReturn(expected);

        ResponseEntity<List<Squad>> result = subject.findAll();

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expected, result.getBody());
    }

    @Test
    public void findsOneAsExpected() throws Exception {
        Squad expected = new Squad();
        when(mockManager.findOne(bogusId)).thenReturn(expected);

        ResponseEntity<Squad> result = subject.get(bogusId);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expected, result.getBody());
    }

    @Test
    public void deletesASquadAsExpected() throws Exception {
        ResponseEntity result = subject.delete(bogusId);

        verify(mockManager).delete(bogusId);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void createsASquadAsExpected() throws Exception {
        Squad fakeSquad = new Squad();
        SquadRequest request = new SquadRequest();
        when(mockManager.create(request)).thenReturn(fakeSquad);

        ResponseEntity<Squad> result = subject.createSquad(request);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(fakeSquad, result.getBody());
    }

    @Test
    public void updatesASquadAsExpected() throws Exception {
        Squad fakeSquad = new Squad();
        SquadRequest request = new SquadRequest();
        when(mockManager.save(bogusId, request)).thenReturn(fakeSquad);

        ResponseEntity<Squad> result = subject.updateSquad(bogusId, request);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(fakeSquad, result.getBody());
    }
}