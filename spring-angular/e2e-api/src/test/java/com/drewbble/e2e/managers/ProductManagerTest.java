package com.drewbble.e2e.managers;

import com.drewbble.e2e.dto.GraphNodeDTO;
import com.drewbble.e2e.dto.GraphNodeLinkDTO;
import com.drewbble.e2e.dto.ProductRequest;
import com.drewbble.e2e.entities.Product;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.repositories.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class ProductManagerTest {
    @Mock
    private ProductRepository mockProductRepository;

    @Mock
    private SquadManager mockSquadManager;

    @InjectMocks
    private ProductManager subject;

    private Product fakeProduct;
    private static final String bogusId = "bogusProductId";

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        fakeProduct = new Product();
        when(mockProductRepository.findOne(bogusId)).thenReturn(fakeProduct);
        when(mockProductRepository.save(fakeProduct)).thenReturn(fakeProduct);
    }

    @Test
    public void findOneWorksAsExpected() throws Exception {
        assertEquals(fakeProduct, subject.findOne(bogusId));
    }

    @Test
    public void findAllWorksAsExpected() throws Exception {
        List<Product> fakeProductsList = new ArrayList<>();
        when(mockProductRepository.findAll()).thenReturn(fakeProductsList);

        List<Product> result = subject.findAll();

        assertEquals(fakeProductsList, result);
    }

    @Test
    public void getSquadsReturnsTheListOfSquads() throws Exception {
        List<Squad> someSquadList = new ArrayList<>();
        fakeProduct.setSquads(someSquadList);

        List<Squad> result = subject.getSquads(bogusId);

        assertEquals(someSquadList, result);
    }

    @Test
    public void whenInvalidIdPassedFindSquadsReturnsNull() throws Exception {
        assertNull(subject.getSquads("invalidId"));
    }

    @Test
    public void whenSavingItSavesTheBasicsCorrectly() throws Exception {
        ProductRequest fakeRequest = getFakeRequest();

        Product result = subject.save(bogusId, fakeRequest);

        assertRequestMatchesProduct(fakeRequest, result);
    }

    @Test
    public void whenSavingItKeepsTheImageIfNoNewImageProvided() throws Exception {
        ProductRequest fakeRequest = getFakeRequest();
        fakeRequest.setImageUrl(null);
        fakeProduct.setImageUrl("alreadySavedImage");

        Product result = subject.save(bogusId, fakeRequest);

        assertEquals("alreadySavedImage", result.getImageUrl());
    }

    @Test
    public void whenSavingItDoesNothingIfAnInvalidIdIsProvided() throws Exception {
        assertNull(subject.save("invalidId", getFakeRequest()));
        verify(mockProductRepository, never()).save(any(Product.class));
    }

    @Test
    public void whenCreatingANewProductMapsAccordingly() throws Exception {
        when(mockProductRepository.save(any(Product.class))).thenReturn(fakeProduct);
        ProductRequest fakeRequest = getFakeRequest();

        Product result = subject.create(fakeRequest);

        assertNotNull(result);
        ArgumentCaptor<Product> captor = ArgumentCaptor.forClass(Product.class);
        verify(mockProductRepository).save(captor.capture());
        assertRequestMatchesProduct(fakeRequest, captor.getValue());
    }

    @Test
    public void deleteWillDeleteById() throws Exception {
        subject.delete(bogusId);

        verify(mockProductRepository).delete(bogusId);
    }

    @Test
    public void addingASquadCallsAppropriateSquadMethod() throws Exception {
        Product result = subject.addSquad(bogusId, "someSquadId");

        assertEquals(fakeProduct, result);
        verify(mockSquadManager).addProduct("someSquadId", bogusId);
    }

    @Test
    public void whenRemovingAllSquadsItClearsTheSquadsAndSaves() throws Exception {
        List<Squad> someListOfSquads = new ArrayList<>(Arrays.asList(new Squad(), new Squad()));
        fakeProduct.setSquads(someListOfSquads);

        Product result = subject.removeAllSquads(bogusId);

        assertEquals(fakeProduct, result);
        assertTrue(fakeProduct.getSquads().isEmpty());
        verify(mockProductRepository).save(fakeProduct);
    }

    @Test
    public void whenPassedAnInvalidIdTheRemoveAllSquadsDoesNothing() throws Exception {
        assertNull(subject.removeAllSquads("invalidId"));
    }

    @Test
    public void whenGettingAProductDTOItMapsAccordingly() throws Exception {
        fakeProduct.setTitle("title");
        fakeProduct.setImageUrl("imageUrl");
        fakeProduct.setDescription("description");

        GraphNodeDTO result = subject.getProductDTO(bogusId);

        assertDTOMatchesProduct(result, fakeProduct);
    }

    @Test
    public void whenNoProductIsFoundReturnsANullDTO() throws Exception {
        assertNull(subject.getProductDTO("invalidId"));
    }

    @Test
    public void whenGettingAllProductsAsDTOObjectsMapsAccordinglyExceptForTheImage() throws Exception {
        List<Product> allProducts = setupMultipleFakeProducts();

        List<GraphNodeDTO> allProductsDTO = subject.getAllProductsDTO();

        assertEquals(allProducts.size(), allProductsDTO.size());
        IntStream.range(0, allProducts.size())
                .forEach(index -> assertDTOMatchesProductExceptForTheImage(allProductsDTO.get(index), allProducts.get(index)));
    }

    @Test
    public void whenGettingAllProductLinksItFindsAllLinksForEachProductThenCreatesThoseAsLinks() throws Exception {
        List<Product> allProducts = setupMultipleFakeProducts();

        List<GraphNodeLinkDTO> allProductsLinks = subject.getAllProductsLinks();

        assertEquals(allProducts.size() * 2, allProductsLinks.size());
        allProducts.forEach(product -> assertProductLinksExist(product, allProductsLinks));
    }

    private void assertProductLinksExist(Product product, List<GraphNodeLinkDTO> allProductsLinks) {
        product.getSquads().forEach(squad -> {
            assertTrue(allProductsLinks.stream().anyMatch(
                    link -> squad.getSquadId().equals(link.getSource()) &&
                            product.getProductId().equals(link.getTarget()) &&
                            "team".equals(link.getSourceType())));
        });
    }

    private List<Product> setupMultipleFakeProducts() {
        List<Product> allProducts = Arrays.asList(createSampleProduct(1), createSampleProduct(2), createSampleProduct(3));
        when(mockProductRepository.findAll()).thenReturn(allProducts);
        return allProducts;
    }

    private Product createSampleProduct(Integer uniquenessNumber) {
        Product product = new Product();
        product.setProductId(uniquenessNumber.toString());
        product.setTitle("title" + uniquenessNumber);
        product.setImageUrl("imageUrl" + uniquenessNumber);
        product.setDescription("description" + uniquenessNumber);
        product.setSquads(createTwoFakeSquadsInAList());
        return product;
    }

    private List<Squad> createTwoFakeSquadsInAList() {
        return Arrays.asList(getFakeSquad(1), getFakeSquad(2));
    }

    private Squad getFakeSquad(Integer someId) {
        Squad squad = new Squad();
        squad.setSquadId(someId.toString());
        return squad;
    }

    private void assertDTOMatchesProductExceptForTheImage(GraphNodeDTO dto, Product product) {
        assertEquals(product.getTitle(), dto.getTitle());
        assertEquals(product.getDescription(), dto.getDescription());
        assertEquals("product", dto.getNodeType());
    }

    private void assertDTOMatchesProduct(GraphNodeDTO dto, Product product) {
        assertDTOMatchesProductExceptForTheImage(dto, product);
        assertEquals(product.getImageUrl(), dto.getImageUrl());
    }

    private ProductRequest getFakeRequest() {
        ProductRequest request = new ProductRequest();
        request.setTitle("title");
        request.setImageUrl("imageUrl");
        request.setDescription("description");
        return request;
    }

    private void assertRequestMatchesProduct(ProductRequest request, Product product) {
        assertEquals(request.getTitle(), product.getTitle());
        assertEquals(request.getImageUrl(), product.getImageUrl());
        assertEquals(request.getDescription(), product.getDescription());
    }
}