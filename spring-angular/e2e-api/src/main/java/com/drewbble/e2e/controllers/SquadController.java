package com.drewbble.e2e.controllers;

import com.drewbble.e2e.dto.SquadRequest;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.managers.SquadManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SquadController {
    private final SquadManager squadManager;

    @Autowired
    public SquadController(SquadManager squadManager) {
        this.squadManager = squadManager;
    }

    @GetMapping("/squads")
    @PreAuthorize("hasAnyRole('admin', 'dev', 'uuidEdit')")
    ResponseEntity<List<Squad>> findAll() {
        return new ResponseEntity<>(squadManager.findAll(), HttpStatus.OK);
    }

    @GetMapping("/squads/{squadId}")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<Squad> get(@PathVariable String squadId) {
        return new ResponseEntity<>(squadManager.findOne(squadId), HttpStatus.OK);
    }

    @DeleteMapping("/squads/{squadId}")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity delete(@PathVariable String squadId) {
        squadManager.delete(squadId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/squads")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<Squad> createSquad(@RequestBody SquadRequest squad) {
        return new ResponseEntity<>(squadManager.create(squad), HttpStatus.OK);
    }

    @PostMapping("/squads/{squadId}")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<Squad> updateSquad(@PathVariable String squadId, @RequestBody SquadRequest squad) {
        return new ResponseEntity<>(squadManager.save(squadId, squad), HttpStatus.OK);
    }
}
