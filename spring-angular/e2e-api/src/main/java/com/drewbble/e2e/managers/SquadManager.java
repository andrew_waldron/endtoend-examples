package com.drewbble.e2e.managers;

import com.drewbble.e2e.dto.GraphNodeDTO;
import com.drewbble.e2e.dto.GraphNodeLinkDTO;
import com.drewbble.e2e.dto.SquadRequest;
import com.drewbble.e2e.entities.Person;
import com.drewbble.e2e.entities.Product;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.repositories.SquadRepository;
import org.h2.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SquadManager {
    @Autowired
    private SquadRepository squadRepository;

    @Autowired
    private PersonManager personManager;

    @Autowired
    private ProductManager productManager;

    public List<Squad> findAll() {
        return squadRepository.findAll();
    }

    public Squad findOne(String squadId) {
        return squadRepository.findOne(squadId);
    }

    public Squad save(String squadId, SquadRequest squadDTO) {
        Squad squadDomain = squadId != null ? findOne(squadId) : null;
        if (squadDomain == null) {
            return null;
        }

        return mapAndSave(squadDTO, squadDomain);
    }

    public Squad create(SquadRequest squadDTO) {
        return mapAndSave(squadDTO, new Squad());
    }

    private Squad mapAndSave(SquadRequest squadDTO, Squad squadDomain) {
        squadDTO.setImageUrl(StringUtils.isNullOrEmpty(squadDTO.getImageUrl()) ? squadDomain.getImageUrl() : squadDTO.getImageUrl());
        BeanUtils.copyProperties(squadDTO, squadDomain);
        return squadRepository.save(squadDomain);
    }

    public void delete(String squadId) {
        squadRepository.delete(squadId);
    }

    Squad addProduct(String squadId, String productId) {
        Squad currentSquad = findOne(squadId);
        if (currentSquad == null) {
            return null;
        }

        List<Product> currentProducts = currentSquad.getProducts();
        if (currentProducts.stream().anyMatch(product -> productId.equalsIgnoreCase(product.getProductId()))) {
            return currentSquad;
        }

        Product productToAdd = productManager.findOne(productId);
        if (productToAdd == null) {
            return currentSquad;
        }

        currentProducts.add(productToAdd);
        squadRepository.save(currentSquad);
        return currentSquad;
    }

    Squad addPerson(String squadId, String personId) {
        Squad currentSquad = findOne(squadId);
        if (currentSquad == null) {
            return null;
        }

        List<Person> currentMembers = currentSquad.getSquadMembers();
        if (currentMembers.stream().anyMatch(person -> personId.equalsIgnoreCase(person.getPersonId()))) {
            return currentSquad;
        }

        Person personToAdd = personManager.findOne(personId);
        if (personToAdd == null) {
            return currentSquad;
        }

        currentMembers.add(personToAdd);
        squadRepository.save(currentSquad);
        return currentSquad;
    }

    public GraphNodeDTO getSquadDTO(String squadId) {
        Squad squad = findOne(squadId);
        if (squad == null) return null;
        return new GraphNodeDTO(squad.getSquadId(), squad.getTitle(), squad.getDescription(), squad.getImageUrl(), "team");
    }

    public List<GraphNodeDTO> getAllSquadsDTO() {
        return findAll().stream()
                .map(squad -> new GraphNodeDTO(squad.getSquadId(), squad.getTitle(), squad.getDescription(), squad.getImageUrl(), "team"))
                .collect(Collectors.toList());

    }

    public List<GraphNodeLinkDTO> getAllSquadsLinks() {
        return findAll().stream()
                .map(squad -> new GraphNodeLinkDTO("1", squad.getSquadId(), "org"))
                .collect(Collectors.toList());
    }
}
