package com.drewbble.e2e.repositories;

import com.drewbble.e2e.entities.Squad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SquadRepository extends JpaRepository<Squad, String> {
}
