package com.drewbble.e2e.controllers;

import com.drewbble.e2e.dto.GraphNodeDTO;
import com.drewbble.e2e.dto.GraphNodeLinkDTO;
import com.drewbble.e2e.dto.Organization;
import com.drewbble.e2e.managers.PersonManager;
import com.drewbble.e2e.managers.ProductManager;
import com.drewbble.e2e.managers.SquadManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class D3GraphController {
    private final PersonManager personManager;
    private final ProductManager productManager;
    private final SquadManager squadManager;

    @Value("${fuse.image.base64}")
    private String fuseImage;

    public String getFuseImage() {
        return fuseImage;
    }

    public void setFuseImage(String fuseImage) {
        this.fuseImage = fuseImage;
    }

    @Autowired
    public D3GraphController(PersonManager personManager, ProductManager productManager, SquadManager squadManager) {
        this.personManager = personManager;
        this.productManager = productManager;
        this.squadManager = squadManager;
    }

    @GetMapping("/d3graph")
    public Organization getEntireGraph() {
        Organization fuseDTO = new Organization();

        List<GraphNodeDTO> nodes = new ArrayList<>();
        nodes.add(new GraphNodeDTO("1", "Fuse", "Fuse by Cardinal Health", fuseImage, "org"));
        nodes.addAll(squadManager.getAllSquadsDTO());
        nodes.addAll(personManager.getAllPeopleDTO());
        nodes.addAll(productManager.getAllProductsDTO());
        fuseDTO.setNodes(nodes);

        List<GraphNodeLinkDTO> links = new ArrayList<>();
        links.addAll(squadManager.getAllSquadsLinks());
        links.addAll(personManager.getAllPeopleLinks());
        links.addAll(productManager.getAllProductsLinks());
        fuseDTO.setLinks(links);

        return fuseDTO;
    }

    @GetMapping("/d3graph/{entityId}")
    public GraphNodeDTO getNodeDetails(@PathVariable String entityId) {
        GraphNodeDTO dto = personManager.getPersonDTO(entityId);
        if (dto == null) dto = productManager.getProductDTO(entityId);
        if (dto == null) dto = squadManager.getSquadDTO(entityId);
        return dto;
    }
}
