package com.drewbble.e2e.dto;

public class GraphNodeLinkDTO {
    private String source;
    private String target;
    private String sourceType;

    public GraphNodeLinkDTO(String sourceId, String target, String sourceType) {
        this.source = sourceId;
        this.target = target;
        this.sourceType = sourceType;
    }

    public String getSourceType() {
        return sourceType;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }
}
