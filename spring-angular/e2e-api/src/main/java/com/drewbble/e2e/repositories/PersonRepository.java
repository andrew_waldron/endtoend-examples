package com.drewbble.e2e.repositories;

import com.drewbble.e2e.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, String> {
    Person findOneByShareableId(String shareableId);
}
