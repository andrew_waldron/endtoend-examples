package com.drewbble.e2e.dto;

import java.util.List;

public class Organization {
    private List<GraphNodeDTO> nodes;
    private List<GraphNodeLinkDTO> links;

    public List<GraphNodeDTO> getNodes() {
        return nodes;
    }

    public void setNodes(List<GraphNodeDTO> nodes) {
        this.nodes = nodes;
    }

    public List<GraphNodeLinkDTO> getLinks() {
        return links;
    }

    public void setLinks(List<GraphNodeLinkDTO> links) {
        this.links = links;
    }
}
