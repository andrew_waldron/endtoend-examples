package com.drewbble.e2e.managers;

import com.drewbble.e2e.dto.GraphNodeDTO;
import com.drewbble.e2e.dto.GraphNodeLinkDTO;
import com.drewbble.e2e.dto.PersonRequest;
import com.drewbble.e2e.entities.Person;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.repositories.PersonRepository;
import org.h2.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PersonManager {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private SquadManager squadManager;

    public Person findOne(String personId) {
        return personRepository.findOne(personId);
    }

    public Person findBySharedId(String sharedId) {
        return personRepository.findOneByShareableId(sharedId);
    }

    public List<Person> findAll() {
        return personRepository.findAll();
    }

    public List<Squad> getSquadsBySharedId(String shareableId) {
        Person person = findBySharedId(shareableId);
        if (person == null) return null;
        return person.getSquads();
    }

    public List<Squad> getSquads(String personId) {
        Person person = findOne(personId);
        if (person == null) return null;
        return person.getSquads();
    }

    public Person save(String personId, PersonRequest personDTO) {
        Person personDomain = personId != null ? findOne(personId) : null;
        if (personDomain == null) return null;
        return savePersonUpdates(personDomain, personDTO);
    }

    public Person create(PersonRequest personDTO) {
        return savePersonUpdates(new Person(), personDTO);
    }

    public Person saveByShareableId(String shareableId, PersonRequest personDTO) {
        Person personDomain = shareableId != null ? findBySharedId(shareableId) : null;
        if (personDomain == null) return null;
        return savePersonUpdates(personDomain, personDTO);
    }

    public GraphNodeDTO getPersonDTO(String personId) {
        Person person = findOne(personId);
        if (person == null) return null;
        return new GraphNodeDTO(person.getPersonId(), person.getTitle(), person.getDescription(), person.getImageUrl(), "people");
    }

    public List<GraphNodeDTO> getAllPeopleDTO() {
        return findAll().stream()
                .map(person -> new GraphNodeDTO(person.getPersonId(), person.getTitle(), person.getDescription(), null, "people"))
                .collect(Collectors.toList());
    }

    public void delete(String personId) {
        personRepository.delete(personId);
    }

    public Person addSquad(String personId, String squadId) {
        squadManager.addPerson(squadId, personId);
        return findOne(personId);
    }

    public Person addSquadByShareableId(String shareableId, String squadId) {
        Person person = findBySharedId(shareableId);
        if (person == null) return null;
        return addSquad(person.getPersonId(), squadId);
    }

    public Person removeAllSquadsByShareableId(String shareableId) {
        return clearSquads(findBySharedId(shareableId));
    }

    public Person removeAllSquads(String personId) {
        return clearSquads(findOne(personId));
    }

    public List<GraphNodeLinkDTO> getAllPeopleLinks() {
        return findAll().stream()
                .map(person -> person.getSquads().stream()
                        .map(squad -> new GraphNodeLinkDTO(squad.getSquadId(), person.getPersonId(), "team"))
                        .collect(Collectors.toList())).flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private Person clearSquads(Person person) {
        if (person == null) return null;
        person.getSquads().clear();
        return personRepository.save(person);
    }

    private Person savePersonUpdates(Person personDomain, PersonRequest personDTO) {
        personDTO.setImageUrl(StringUtils.isNullOrEmpty(personDTO.getImageUrl()) ? personDomain.getImageUrl() : personDTO.getImageUrl());
        BeanUtils.copyProperties(personDTO, personDomain);
        return personRepository.save(personDomain);
    }
}
