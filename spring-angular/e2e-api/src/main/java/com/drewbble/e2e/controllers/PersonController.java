package com.drewbble.e2e.controllers;

import com.drewbble.e2e.dto.PersonRequest;
import com.drewbble.e2e.entities.Person;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.managers.PersonManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class PersonController {
    private final PersonManager personManager;

    @Autowired
    public PersonController(PersonManager personManager) {
        this.personManager = personManager;
    }

    @GetMapping("/persons")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<List<Person>> getAll() {
        return new ResponseEntity<>(personManager.findAll(), HttpStatus.OK);
    }

    @PostMapping("/persons")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<Person> createPerson(@RequestBody PersonRequest person) {
        return new ResponseEntity<>(personManager.create(person), HttpStatus.OK);
    }

    @DeleteMapping("/persons/{personId}")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity delete(@PathVariable String personId) {
        personManager.delete(personId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/persons/{personId}")
    @PreAuthorize("hasAnyRole('admin', 'dev', 'uuidEdit')")
    ResponseEntity<Person> get(@PathVariable String personId, @RequestParam(value = "isShareableId", required = false) boolean isShareableId, HttpServletRequest request) {
        boolean useShareableId = request.isUserInRole("uuidEdit") || isShareableId;
        Person response = useShareableId ? personManager.findBySharedId(personId) : personManager.findOne(personId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/persons/{personId}/squads")
    @PreAuthorize("hasAnyRole('admin', 'dev', 'uuidEdit')")
    ResponseEntity<List<Squad>> getSquads(@PathVariable String personId, @RequestParam(value = "isShareableId", required = false) boolean isShareableId, HttpServletRequest request) {
        boolean useShareableId = request.isUserInRole("uuidEdit") || isShareableId;
        List<Squad> response = useShareableId ? personManager.getSquadsBySharedId(personId) : personManager.getSquads(personId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/persons/{personId}")
    @PreAuthorize("hasAnyRole('admin', 'dev', 'uuidEdit')")
    ResponseEntity<Person> updatePerson(@PathVariable String personId, @RequestBody PersonRequest person, @RequestParam(value = "isShareableId", required = false) boolean isShareableId, HttpServletRequest request) {
        boolean useShareableId = request.isUserInRole("uuidEdit") || isShareableId;
        Person response = useShareableId ? personManager.saveByShareableId(personId, person) : personManager.save(personId, person);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/persons/{personId}/squads")
    @PreAuthorize("hasAnyRole('admin', 'dev', 'uuidEdit')")
    ResponseEntity<Person> deleteAllSquads(@PathVariable String personId, @RequestParam(value = "isShareableId", required = false) boolean isShareableId, HttpServletRequest request) {
        boolean useShareableId = request.isUserInRole("uuidEdit") || isShareableId;
        Person response = useShareableId ? personManager.removeAllSquadsByShareableId(personId) : personManager.removeAllSquads(personId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/persons/{personId}/squads/{squadId}")
    @PreAuthorize("hasAnyRole('admin', 'dev', 'uuidEdit')")
    ResponseEntity<Person> addSquad(@PathVariable("personId") String personId, @PathVariable("squadId") String squadId, @RequestParam(value = "isShareableId", required = false) boolean isShareableId, HttpServletRequest request) {
        boolean useShareableId = request.isUserInRole("uuidEdit") || isShareableId;
        Person response = useShareableId ? personManager.addSquadByShareableId(personId, squadId) : personManager.addSquad(personId, squadId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
