package com.drewbble.e2e.controllers;

import com.drewbble.e2e.dto.ProductRequest;
import com.drewbble.e2e.entities.Product;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.managers.ProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    private final ProductManager productManager;

    @Autowired
    public ProductController(ProductManager productManager) {
        this.productManager = productManager;
    }

    @GetMapping("/products/{productId}")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<Product> get(@PathVariable String productId) {
        return new ResponseEntity<>(productManager.findOne(productId), HttpStatus.OK);
    }

    @PostMapping("/products")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<Product> createProduct(@RequestBody ProductRequest product) {
        return new ResponseEntity<>(productManager.create(product), HttpStatus.OK);
    }

    @PostMapping("/products/{productId}")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<Product> updateProduct(@PathVariable String productId, @RequestBody ProductRequest product) {
        return new ResponseEntity<>(productManager.save(productId, product), HttpStatus.OK);
    }

    @DeleteMapping("/products/{productId}")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity delete(@PathVariable String productId) {
        productManager.delete(productId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/products")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<List<Product>> getAll() {
        return new ResponseEntity<>(productManager.findAll(), HttpStatus.OK);
    }

    @GetMapping("/products/{productId}/squads")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<List<Squad>> getSquads(@PathVariable String productId) {
        return new ResponseEntity<>(productManager.getSquads(productId), HttpStatus.OK);
    }

    @DeleteMapping("/products/{productId}/squads")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<Product> deleteAllSquads(@PathVariable String productId) {
        return new ResponseEntity<>(productManager.removeAllSquads(productId), HttpStatus.OK);
    }

    @PostMapping("/products/{productId}/squads/{squadId}")
    @PreAuthorize("hasAnyRole('admin', 'dev')")
    ResponseEntity<Product> addSquad(@PathVariable("productId") String productId, @PathVariable("squadId") String squadId) {
        return new ResponseEntity<>(productManager.addSquad(productId, squadId), HttpStatus.OK);
    }
}