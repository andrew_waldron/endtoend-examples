package com.drewbble.e2e.repositories;

import com.drewbble.e2e.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String> {
}
