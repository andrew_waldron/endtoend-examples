package com.drewbble.e2e.dto;

public class GraphNodeDTO {
    private String id;
    private String title;
    private String description;
    private String imageUrl;
    private String nodeType;

    public GraphNodeDTO(String id, String title, String description, String imageUrl, String nodeType) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.nodeType = nodeType;
    }

    public String getNodeType() {
        return nodeType;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

}
