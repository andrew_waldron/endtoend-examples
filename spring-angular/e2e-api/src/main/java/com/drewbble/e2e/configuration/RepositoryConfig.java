package com.drewbble.e2e.configuration;

import com.drewbble.e2e.entities.Person;
import com.drewbble.e2e.entities.Product;
import com.drewbble.e2e.entities.Squad;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Person.class, Product.class, Squad.class);
    }
}