package com.drewbble.e2e.managers;

import com.drewbble.e2e.dto.GraphNodeDTO;
import com.drewbble.e2e.dto.GraphNodeLinkDTO;
import com.drewbble.e2e.dto.ProductRequest;
import com.drewbble.e2e.entities.Product;
import com.drewbble.e2e.entities.Squad;
import com.drewbble.e2e.repositories.ProductRepository;
import org.h2.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductManager {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SquadManager squadManager;

    public Product findOne(String productId) {
        return productRepository.findOne(productId);
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public List<Squad> getSquads(String productId) {
        Product product = findOne(productId);
        if (product == null) return null;
        return product.getSquads();
    }

    public Product save(String productId, ProductRequest productDTO) {
        Product productDomain = productId != null ? findOne(productId) : null;
        if (productDomain == null) return null;
        return mapAndSave(productDTO, productDomain);
    }

    public Product create(ProductRequest productDTO) {
        return mapAndSave(productDTO, new Product());
    }

    private Product mapAndSave(ProductRequest productDTO, Product product) {
        productDTO.setImageUrl(StringUtils.isNullOrEmpty(productDTO.getImageUrl()) ? product.getImageUrl() : productDTO.getImageUrl());
        BeanUtils.copyProperties(productDTO, product);
        return productRepository.save(product);
    }

    public void delete(String productId) {
        productRepository.delete(productId);
    }

    public Product addSquad(String productId, String squadId) {
        squadManager.addProduct(squadId, productId);
        return findOne(productId);
    }

    public Product removeAllSquads(String productId) {
        Product product = findOne(productId);
        if (product == null) return null;
        product.getSquads().clear();
        return productRepository.save(product);
    }

    public GraphNodeDTO getProductDTO(String productId) {
        Product product = findOne(productId);
        if (product == null) return null;
        return new GraphNodeDTO(product.getProductId(), product.getTitle(), product.getDescription(), product.getImageUrl(), "product");
    }

    public List<GraphNodeDTO> getAllProductsDTO() {
        return findAll().stream()
                .map(product -> new GraphNodeDTO(product.getProductId(), product.getTitle(), product.getDescription(), null, "product"))
                .collect(Collectors.toList());
    }

    public List<GraphNodeLinkDTO> getAllProductsLinks() {
        return findAll().stream()
                .map(product -> product.getSquads().stream()
                        .map(squad -> new GraphNodeLinkDTO(squad.getSquadId(), product.getProductId(), "team"))
                        .collect(Collectors.toList())).flatMap(Collection::stream)
                .collect(Collectors.toList());
    }
}
