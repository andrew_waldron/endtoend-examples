#!/usr/bin/env bash

cd ../e2e-web/

npm install
gulp configDev
gulp build

cd -

cf push -f fusewall-web.yml
